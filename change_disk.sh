#!/bin/bash
#
# vim: tabstop=3

#title           :change_disk.sh
#description     :This script will change a disk
#author          :Cédrick GAILLARD
#date            :20160420
#version         :0.1
#==============================================================================

# NOTE:
# We assume the disk prefix is "disk"
# We assume the ssd prefix is "ssd"

# you can change your rings name
ring_data=DATA
ring_meta=META

####################
### internal functions
####################
Usage()
{
	cat << EOF
	# -f __________ to force the formatting even on already sliced disk
	# --hpraid ____ to detect and create a raid0 (Work In Progress)
	# --reload ____ to just reload the disk, no format
	# --scan_scsi _ will just rescan the scsi chain on the system then exit
	# --dryrun ____ display the list of commands that will be used, without running them
	Usage example:
	$0 --scan_scsi
	$0 disk4 /dev/sdg
EOF
}

debug()
{
	if [ -n "$DEBUG" ] ; then
		echo "${@}"
	else
		${@}
	fi
}

# HP controller: get confs
hp_getconf()
{
	# Ctrl list
	$hp_cli ctrl all show |sed 's@(.*@@' |grep -v "^$" |awk '{print $NF}' > "$hp_list_controllers"

	# Scan for new devices
	$hp_cli rescan
	
	cp /dev/null "$list_new_disks"
	for slt in $(< "$hp_list_controllers")
	do
		#echo "Disable write cache if no battery for ctrl $slt"
		######$hp_cli ctrl slot=2 modify nobatterywritecache=disable
		$hp_cli ctrl slot="$slt" show config > "${hp_show_conf}.slot_$slt"
		awk '/unassigned/,/*/' "${hp_show_conf}.slot_$slt" |grep physicaldrive |sed "s@^@slot=${slt} @" >> "$list_new_disks"
	done
}

# Rescan the scsi chain
scan_scsi()
{
	for i in /sys/class/scsi_host/host*/scan ; do echo '- - -' > "$i" 2>/dev/null ; done
}

# HP controller: add disks to raid0
hp_add2raid()
{
	# output example from list_new_disks:
	# slot=1       physicaldrive 1I:1:13 (port 1I:box 1:bay 13, Solid State SATA, 800 GB, OK)
	# slot=1       physicaldrive 2I:1:1 (port 2I:box 1:bay 1, SAS, 6001.1 GB, OK)
	list_type=$(awk -F, '{print $2, $3}' "$list_new_disks" |sort |uniq -c)

	# cat "${list_new_disks}"
	echo "count:"
	echo "$list_type"

	echo -n "Is it OK to add these disks to a raid0 group? [y|N] "
	read -r answer

	# Check the disks or exit
	answer=$(echo "$answer" |awk '{print tolower($0)}')
	case $answer
	in
		"y"|"yes") echo ;;
		*) echo "This answer was not what i expected :/" && exit 1 ;;
	esac

	echo "Add the disks to Raid0 volume in progress..."
	while read -r slot atype disk rest
	do
		debug $hp_cli ctrl "$slot" create type=ld drives="$disk" raid=0
		echo "$disk done"
	done < "$list_new_disks"

	echo "Disks added, rescan"
	debug $hp_cli rescan
}

# HP controller: list free disks to format
hp_list_new_disks()
{
	echo "Identify new volumes"
	$hp_cli ctrl all show config detail > "${hp_show_conf_detail}"

	grep -A17 'Disk Name' "$hp_show_conf_detail" | \
		egrep 'Disk Name|physicaldrive' | \
		awk '{
			if ($1 ~ /Disk/) {
				print $3
			} else {
				print $2
			}
		}' |sed ':a;N;s/\n/ /g' > "$list_all_disks"

	cp /dev/null "$to_be_formatted"
	while read -r pdev phys
	do
		for new in $(awk '{print $3}' "$list_new_disks")
		do
			[ "$phys" = "$new" ] && {
				# Double check, do not want to format an already sliced disk
				slice=$(ls -l "/sys/block/${pdev}/"*/dev 2>/dev/null)
				if [ -z "$slice" ]; then
					echo "$pdev $phys" >> "$to_be_formatted"
				else
					echo "Error detected, $pdev $phys contains slice(s), cannot work on it"
					exit 1
				fi
			}
		done < "$list_all_disks"
	done

	echo "Disk free to be formatted:"
	cat "$to_be_formatted"
	echo -n "Do you agree? [y|N] "

	read -r ok_for_format
	ok_for_format=$(echo "$ok_for_format" |awk '{print tolower($0)}')
	case $ok_for_format
	in
		"y"|"yes") echo ;;
		*) echo "Abandon" && exit 2 ;;
	esac
}

# HP controller: detect new disks, add them to a raid 0
hpraidconfig()
{
	hp_getconf
	hp_add2raid
	hp_list_new_disks

	[ "$ring_name" = "$ring_data" ] && {
		for slt in $(< "$hp_list_controllers")
		do
			echo "Enable caching for slot $slt"
			debug $hp_cli ctrl slot="$slt" ld all modify arrayaccelerator=enable
		done
	}

	hp_list_new_disks
}

# Format the device in argument
format()
{
	disk=$1
	debug parted -s -a optimal "$disk" mklabel gpt || exit 1
	debug parted -s -a optimal "$disk" mkpart primary ext4 1mb -- -1 || exit 1
	debug mke2fs -t ext4 -F -m0 "${disk}1" || exit 1
	debug blockdev --rereadpt "${disk}" 2>/dev/null
	debug tune2fs -c0 -m0 -i0 "${disk}1"
	debug tune2fs -l "${disk}1" |grep UUID |awk '{print $NF}'
}

####################
### END internal functions
####################

# restore the bizobjs if needed
# arg #1: ssd disk number (eg. ssd2)
# arg #2: data ring name (eg. DATA)
# desc: if some bizobjs are hosted on this ssd, and if we have backups
# the backups will be restored and reconciliated
restore_bizobjs()
{
	if [ "$#" -ne "2" ] ; then
		echo "bad number of arguments for the restore_bizobjs function"
		exit 1
	fi
	scal_disk=$1
	ring_data=$2

	# Identify which HDDs have theirs bizobjs on the ssd
	bizobj_disks=$(bizobjs_on_ssd "$scal_disk")
	if [ -z "$bizobj_disks" ] ; then
		return 0
	fi

	# stop all disks
	for disk in $bizobj_disks ; do debug scality-iod stop "$disk" ; done

	# create the paths on ssd
	for disk in $bizobj_disks
	do
		debug mkdir -pv "$(sed -n "s/^nvp=//p" "/etc/biziod/bizobj.$disk")"
	done

	# restore the last backups if any
	for disk in $bizobj_disks
	do
		backup_dir=$(ls -rtd /scality/"$disk"/bizobj-backup/* 2>/dev/null |tail -1)
		if [ -d "$backup_dir" ] ; then
			bizobj_dest="$(sed -n "s/^nvp=//p" "/etc/biziod/bizobj.$disk")"
			debug cp -r "${backup_dir}"/* "${bizobj_dest}"/

			# reconciliate the data and bizobj.bin
			echo "Bizobj.bin reconciliation"
			debug biziod -dl -P "bp=/scality/$disk;nvp=${bizobj_dest}" -t bizobj recdump "$ring_data" 0 -CHr
			debug biziod -dl -P "bp=/scality/$disk;nvp=${bizobj_dest}" -t bizobj recdump "$ring_data" 0 -CFR
		else
			cat << EOF
			#####
			# No bizobj.bin backup found for $disk, I will move:
			# /scality/${disk}/$ring_data/ in /scality/${disk}/${ring_data}.${V_DATE}
			# You may want to delete these old datas
			#####
EOF
			debug mount /scality/${disk} &>/dev/null
			debug mv "/scality/${disk}/$ring_data/" "/scality/${disk}/${ring_data}.${V_DATE}" 2>/dev/null
			debug mkdir -p "/scality/${disk}/$ring_data/0"
		fi
	done

	# restart the biziods
	for disk in $bizobj_disks ; do debug scality-iod start "$disk" ; done

	# Clear the status on all disks
	for disk in $bizobj_disks
	do
		clear_disk_status $disk $ring_data
	done
}

bizobjs_on_ssd()
{
	ssd=$1 
	grep -l "nvp=/scality/$1/" /etc/biziod/* |awk -F. '{print $NF}' |sort -u
}

sleep_200()
{
 perl -e "select(undef,undef,undef,0.2);"
}

# bizio clear
bizio_clear()
{
	debug bizioctl -c del_mflags -a 0x1 -N "$1" "bizobj://${2}:0" 2>/dev/null ; sleep_200
	debug bizioctl -c del_mflags -a 0x2 -N "$1" "bizobj://${2}:0" 2>/dev/null ; sleep_200
	debug bizioctl -c set_mflags -a 0x0 -N "$1" "bizobj://${2}:0" 2>/dev/null ; sleep_200
	debug bizioctl -c clear_repair_failed -a 1 -N "$1" "bizobj://${2}:0" 2>/dev/null ; sleep_200
}

# Clear the flags and create the bizobj if needed
clear_disk_status()
{
	disk_to_clean=$1
	ring=$2

	# Iterate to force the creation of the bizobj.bin if needed.
	bizio_clear "$disk_to_clean" "${ring}"
	if [ -n "$DEBUG" ] ; then
		m=2
	else
		m=10
	fi
	for max in {1..10}
	do
		[ "$max" -ge "$m" ] && {
			if [ -z "$DEBUG" ] ; then
				cat << EOF >&2
			Unable to create the ${this_nvp}/$ring/0/bizobj.bin for $disk_to_clean
			I am going to abort the operations
			try to fix the issue and use the --reload flag to reload the disk, ie.:
			# $0 --reload $old_disk
EOF
			fi
			exit 1
		}

		# If nvp is not set, the disk hosts its own bizobj
		this_nvp=$(sed -n "s/^nvp=//p" "/etc/biziod/bizobj.$disk_to_clean")
		if [ -z "$this_nvp" ] ; then
			this_nvp="/scality/$disk_to_clean/"
		fi

		if [ ! -e "${this_nvp}/$ring/0/bizobj.bin" ] ; then
			# remove both the nvp and local store if exists
			debug rm -rf "${this_nvp}"/"${ring}"/0/*
			debug rm -rf /scality/"${disk_to_clean}"/"${ring}"/0/*
			debug bizioopen -c -N "$disk_to_clean" "bizobj://${ring}:0"
		else
			break
		fi
	done
	echo "Reload the biziod $disk_to_clean on the $ring nodes"

	myip=$(grep address /etc/sagentd.yaml |awk '{print $2}' |sort -u)
	if [ "$(check_ringsh)" = "false" ] ; then
		cat << EOF >&2
		Ringsh is not installed or configured, please run the following commands on a machine with ringsh configured:
		nodes=\$(ringsh supervisor ringStatus $ring |grep $myip |grep Node: |awk '{print \$2}')
		for node in \$nodes ; do ringsh -r $ring -u \$node node chunkMgrStoreBizioReload $disk_to_clean ; done

EOF
	else
		nodes=$(ringsh supervisor ringStatus "$ring" |grep "$myip" |grep Node: |awk '{print $2}')
		for node in $nodes
		do
			if [ -z "$DEBUG" ] ; then
				ringsh -r "$ring" -u "$node" node chunkMgrStoreBizioReload "$disk_to_clean" >/dev/null
			else
				debug ringsh -r "$ring" -u "$node" node chunkMgrStoreBizioReload "$disk_to_clean"
				sleep_200
			fi
		done
	fi
}

# Check that ringsh is installed and configured
check_ringsh()
{
	if [ -d "/etc/scality-ringsh/conf.d" ] ; then
		ringsh_conf_path=/etc/scality-ringsh/conf.d
	elif [ -d "/usr/local/scality-ringsh/ringsh/conf.d" ] ; then
		ringsh_conf_path=/usr/local/scality-ringsh/ringsh/conf.d
	else
		return 1
	fi

	if [ "$(ls -1 "$ringsh_conf_path" 2>/dev/null |grep -v sample |wc -l)" = "0" ] ; then
		return 1
	fi
}

shrt2long_dev()
{
	disk=$1
	if [ "$(echo "$1" |grep -c "^/dev/")" = "0" ] ; then
		disk="/dev/$1"
	fi

	new_disk_type=$(LANG=C stat -c "%F" "$disk")
	if [ "$new_disk_type" != "block special file" ] ; then
		echo "ERROR: $new_disk is not a device" &>2
		exit 1
	fi
	echo "$disk"
}

####################
### Variables initialisation
####################

tmpdir=$(mktemp -d)

####
# need to handle several raid controlers in the near future
####

DEBUG=

# HP confs
hp_cli="hpssacli"/usr/local/scality-ringsh/ringsh/conf.d/
hp_show_conf="$tmpdir/ctrl_all_sh_conf"
hp_show_conf_detail="$tmpdir/ctrl_all_sh_conf_detail"
hp_list_controllers="$tmpdir/ctrl_list"

list_new_disks="$tmpdir/list_new_disks"
list_all_disks="$tmpdir/all_disks"
to_be_formatted="$tmpdir/2beformat"

V_DATE="$(date "+%Y-%m-%d-%H:%M:%S")"
nb_args=2
disk_reload=false
force=false
raid=false
old_disk=
new_disk=
while [ "$#" -gt "0" ]
do
	case $1
	in
		-f) force=true
			shift 1
			;;
		--hpraid) echo "Not ready yet"
			exit 255
			raid=hp
			nb_args=1
			shift 1
			;;
		--reload) disk_reload=true
			nb_args=1
			shift 1
			;;
		--scan_scsi) scan_scsi
			exit $?
			;;
		--dryrun) DEBUG=true
			shift 1
			;;
		*) # Check that we have good arguments
			if [ "$#" -ne "$nb_args" ] ; then
				echo "wrong arguments number, authorized args are: -f and/or --hpraid followed by the disk number and the new device"
				Usage
				exit 1
			fi

			if [ -n "$old_disk" ] ; then
				echo "Disk already set"
				Usage
				exit 1
			fi
			old_disk="$1"
			new_disk="$2"
			shift "$nb_args"
			;;
	esac
done
if [ -z "$old_disk" ] ; then
	Usage
	exit 1
fi

# are we working on ssd or hdd?
ring_name=$ring_data
[ "$(echo "$old_disk" |cut -c-3)" = "ssd" ] && ring_name="$ring_meta"

# we do accept short name for a device
if [ "$nb_args" = "2" ] ; then
	if [ -z "$new_disk" ] ; then
		Usage
		exit 1
	fi
	new_disk=$(shrt2long_dev "$new_disk")
fi

# rescan all scsi devices
scan_scsi

# HP controller: create the raid0 configuration on hp
if [ "$raid" = "hp" ] ; then
	hpraidconfig

	# disk to format: $to_be_formatted
	nb_disk_to_format=$(wc -l "$to_be_formatted")
	if [ "$nb_disk_to_format" -eq "1" ] ; then
		new_disk=$(< "$to_be_formatted")
	elif [ "$nb_disk_to_format" -gt "1" ] ; then
		nl "$to_be_formatted"
		echo "anything else than a number to abort"
		while true
		do
			read -r choose_disk
			case $choose_disk
			in
				[0-9]*) [ "$choose_disk" -ge "$nb_disk_to_format" ] && break
					new_disk=$(sed "${choose_disk}q;d" "$to_be_formatted" |awk '{print $1}')
					;;
				*) echo "Abandon" && exit 1 ;;
			esac
		done
	else
		echo "no disk found"
		exit 1
	fi
fi

# in case of a simple reload, we do not pass through this block
if [ "$disk_reload" = "false" ] ; then
	# disk should not be sliced
	nbr_part=$(lsblk -n -r "$new_disk" |wc -l)

	# use the -f flag to force the format
	if [ "$nbr_part" -ne "1" ] ; then
		if [ "$force" = "false" ] ; then
			echo "/!\\ disk already contains partition(s) /!\\"
			exit 1
		fi
	fi

	# start to reread the partition table
	debug blockdev --rereadpt "$new_disk" 2>/dev/null

	# be sure the disk is unmounted
	debug scality-iod stop "$old_disk" 2>/dev/null
	debug umount "/scality/$old_disk" 2>/dev/null

	# Identify which HDDs have theirs bizobjs on the ssd
	bizobj_disks=$(bizobjs_on_ssd "$old_disk")
	if [ -n "$bizobj_disks" ] ; then
		# stop all disks
		for disk in $bizobj_disks
		do
			debug scality-iod stop "$disk"
			debug umount "/scality/$disk"
			sleep 1
		done
	fi
	format "$new_disk"

	new_uuid=$(blkid -s UUID -o value "${new_disk}1")
	[ "$?" = "0" ] || {
		echo "cannot get the new UUID"
		exit 1
	}

	if [ -z "$new_uuid" ] ; then
		echo "UUID is not legal"
		debug exit 1
	fi

	# backup the fstab
	debug cp /etc/fstab /etc/fstab.${V_DATE}

	# Update the fstab with the new uuid
	if [ -z "$DEBUG" ] ; then
		sed -i "s@^UUID=.*\([[:space:]]/scality/${old_disk}[[:space:]].*\)@UUID=${new_uuid} \1@" /etc/fstab
	else
		debug sed -i \"s@^UUID=.*\\\(\[\[:space:\]\]/scality/${old_disk}\[\[:space:\]\].*\\\)@UUID=${new_uuid} \\\1@\" /etc/fstab
	fi

	debug mount "/scality/$old_disk" || exit 1
	debug mkdir -p "/scality/$old_disk/$ring_name/0/"

	# If nvp is not set, the disk hosts its own bizobj
	this_nvp=$(sed -n "s/^nvp=//p" "/etc/biziod/bizobj.$old_disk")
	# if nvp is on another place, remove the associated bizobj.bin
	if [ -n "$this_nvp" ] ; then
		if [ -d "${this_nvp}/${ring_name}" ] ; then
			mv "${this_nvp}/${ring_name}" "${this_nvp}/${ring_name}-${V_DATE}"
		fi
	fi

	# Restart the biziod
	debug scality-iod start "$old_disk"
fi

# Clear the flags and create the bizobj if needed
clear_disk_status "$old_disk" "$ring_name"

if [ "$ring_name" = "$ring_meta"  -a "$disk_reload" != "true" ] ; then
	restore_bizobjs "$old_disk" "$ring_data"
fi

echo 'Done!'


