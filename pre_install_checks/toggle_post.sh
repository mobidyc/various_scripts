#!/bin/bash

post=gitHooks/post-checkout
pre=gitHooks/pre-commit

if [ -e "$post" ] ; then
	mv $post $post.disable && echo post disabled
elif [ -e "$post.disable" ] ; then
	mv $post.disable $post && echo post enabled
fi

if [ -e "$pre" ] ; then
	mv $pre $pre.disable && echo pre disabled
elif [ -e "$pre.disable" ] ; then
	mv $pre.disable $pre && echo pre enabled
fi
