#
# Below are the generic tests always loaded at startup, they can be superseded.
# Do not forget to append your objects to the existing (internal) "checks" list.
#

#
# Example of check indirect dependency
# The checked value comes from another check
#
# sys_val = Check("Hyperthreading", category="Hardware", subcategory="Configuration")
# sys_val.to_run("LANG=C lscpu |egrep '^Core|^Socket' |awk 'BEGIN{x=1}{x*=$NF}END{printf x}'")
# sys_val.equal( 'self.val_of("Number of CPUs")', "recommended")
# sys_val.set_description("Hyperthreading should be disabled. test if the sockets*cores matches the number of CPUs, if not, Hyperthreading is enabled")
# checks.append( sys_val )
#
# End Example of check indirect dependency
#

#
# Exemple of explicit dependency
#
#sys_val = Check("SSHD conf: PermitRootLogin", category="System", subcategory="Security")
#sys_val.to_run("parse_sshd_conf('/etc/ssh/sshd_config', 'PermitRootLogin')", type="python")
#sys_val.regex("(?i)Yes|(?i)Without-password", "recommended")
#sys_val.set_description("root user should be able to connect to this server in ssh")
#sys_val.set_condition_id("Current UID")
#checks.append( sys_val )
#
# End of Exemple of explicit dependency
#

#
# Example of auto-generated test.
# create a list with sys_val.set_duplicate()
# iterate the list and replace %%val%%, create an object for each iteration
#
# sys_val = Check("Speed for interface: %%val%%", category="Network", subcategory="status")
# sys_val.to_run("ethtool %%val%% 2>/dev/null |grep 'Speed: ' |sed 's@[^0-9]@@g'", type="shell")
# sys_val.greater_or_eq(10000, "recommended")
# sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_INET")
# sys_val.set_description("Interfaces need to run at 10Gb/s")
# checks.append( sys_val )
#
# End Example of auto-generated test.
#

#
# Example of Information about tests which are not yet implemented
# Do not forget to check them manualy
# Based on the category and subcategory, theses false tests will always be
# run first to be displayed in the header
#
#     sys_val = Check("Check that a raid controller has disks in raid 0", category="TO BE IMPLEMENTED", subcategory="PLEASE CHECK IT MANUALLY")
#     sys_val.to_run("echo false")
#     sys_val.equal("true", "unknown")
#     checks.append( sys_val )
#
# End Example of Information about tests which are not yet implemented
#

Config.default_category = "Hardware"
Config.default_subcategory = "Configuration"

sys_val = Check("Hyperthreading")
sys_val.to_run("LANG=C lscpu |egrep '^Core|^Socket' |awk 'BEGIN{x=1}{x*=$NF}END{printf x}'")
sys_val.equal( 'self.val_of("Number of CPUs")', "important")
sys_val.set_description("Hyperthreading should be disabled. test if the sockets*cores matches the number of CPUs, if not, Hyperthreading is enabled")
sys_val.set_fix("Please enter into the BIOS to disable HyperThreading")
checks.append( sys_val )

sys_val = Check("Operating System bits")
sys_val.to_run("platform.architecture()[0]", type="python")
sys_val.set_description("64Bits architecture is the only one CPU type supported")
sys_val.set_fix("no comment")
sys_val.equal("64bit")
checks.append( sys_val )

sys_val = Check("NUMA Deactivation")
sys_val.to_run("test -e /proc/sys/kernel/numa_balancing ; printf $?")
sys_val.set_description("It is recommended to have NUMA deactivated (Memory interleave needs to be disabled in the BIOS too)")
sys_val.notequal(0, "important")
checks.append( sys_val )

Config.default_category = "Hardware"
Config.default_subcategory = "Physical"

sys_val = Check("Number of CPUs")
sys_val.to_run("multiprocessing.cpu_count()", type="python")
sys_val.greater_or_eq(CHECK_CONFIG.supervisor_cpu_number, "recommended")
sys_val.set_description("Number of CPU seen on the system")
sys_val.set_fix("You should add more CPU on this server")
checks.append( sys_val )

sys_val = Check("Power supply redundancy")
sys_val.to_run("resources/scripts/power_locum.sh", type="shell")
sys_val.greater_or_eq(CHECK_CONFIG.supervisor_ps, "recommended")
sys_val.set_description("At least one power supply needs to be in place.")
sys_val.set_fix("Please add more Power Supply, on dedicated lanes")
checks.append( sys_val )

sys_val = Check("Installed memory")
sys_val.to_run("grep MemTotal: /proc/meminfo |awk '{print $2}'")
sys_val.greater_or_eq(4194304, "important")
sys_val.greater_or_eq(16777216, "recommended")
sys_val.set_description("Minimum 4GB, Recommended 16GB")
sys_val.set_fix("It would be great to increase the physical memory")
checks.append( sys_val )

sys_val = Check("System disk type")
sys_val.to_run("resources/scripts/system_disk_type.sh", type="shell")
sys_val.equal("hdd", "recommended")
sys_val.set_description("The system disk type should not be SSD")
sys_val.set_fix("It is not recommended to have SSD as a system disk")
checks.append( sys_val )

Config.default_category = "Hardware"
Config.default_subcategory = "Perfs"

#sys_val = Check("SSDs devices reads consistency timings")
#sys_val.to_run("resources/scripts/disk_timings.sh --ssd -t |awk '{x = $NF; sub(/%/, \"\", x); print x}'", type="shell")
#sys_val.lesser_or_eq(4, "recommended")
#sys_val.lesser_or_eq(8, "important")
#sys_val.set_condition_id("Hdparm installation")
#sys_val.set_condition_id("Number of free SSDs")
#sys_val.set_description("Test the devices read of all free SSDs (not partitioned) and calculate the consistency, recommended in case of more than 4% of difference, important if more than 8%")
#checks.append( sys_val )

#sys_val = Check("SSDs cache reads consistency timings")
#sys_val.to_run("resources/scripts/disk_timings.sh --ssd -T |awk '{x = $NF; sub(/%/, \"\", x); print x}'", type="shell")
#sys_val.lesser_or_eq(4, "recommended")
#sys_val.lesser_or_eq(8, "important")
#sys_val.set_condition_id("Hdparm installation")
#sys_val.set_condition_id("Number of free SSDs")
#sys_val.set_description("Test the cache read of all free SSDs (not partitioned) and calculate the consistency, recommended in case of more than 4% of difference, important if more than 8%")
#checks.append( sys_val )

#sys_val = Check("HDDs devices reads consistency timings")
#sys_val.to_run("resources/scripts/disk_timings.sh --hdd -t |awk '{x = $NF; sub(/%/, \"\", x); print x}'", type="shell")
#sys_val.lesser_or_eq(4, "recommended")
#sys_val.lesser_or_eq(8, "important")
#sys_val.set_condition_id("Hdparm installation")
#sys_val.set_condition_id("Number of free HDDs")
#sys_val.set_description("Test the devices read of all free HDDs (not partitioned) and calculate the consistency, recommended in case of more than 4% of difference, important if more than 8%")
#checks.append( sys_val )

#sys_val = Check("HDDs cache reads consistency timings")
#sys_val.to_run("resources/scripts/disk_timings.sh --hdd -T |awk '{x = $NF; sub(/%/, \"\", x); print x}'", type="shell")
#sys_val.lesser_or_eq(4, "recommended")
#sys_val.lesser_or_eq(8, "important")
#sys_val.set_condition_id("Hdparm installation")
#sys_val.set_condition_id("Number of free HDDs")
#sys_val.set_description("Test the cache read of all free HDDs (not partitioned) and calculate the consistency, recommended in case of more than 4% of difference, important if more than 8%")
#checks.append( sys_val )

sys_val = Check("Tuned-adm profile")
sys_val.to_run("fgrep -c latency-performance /etc/tuned/active_profile 2>/dev/null", type="shell")
sys_val.equal("1", "recommended")
sys_val.set_description("run 'tuned-adm profile latency-performance' to optimize the system (RHEL/CentOS only)")
sys_val.set_fix("tunedadm", type="script", isauto=True)
checks.append( sys_val )

Config.default_category = "System"
Config.default_subcategory = "HW"

sys_val = Check("IRQ balancing service")
sys_val.to_run("sed 's/#.*//' /etc/default/irqbalance /etc/sysconfig/irqbalance 2>/dev/null |egrep -hw 'IRQBALANCE_ONESHOT|ONESHOT' |cut -d= -f2 |sort -u", type="shell")
sys_val.regex('"?1"?|"?(?i)Yes"?')
sys_val.set_description("Irqbalance should run once at boot then stop")
sys_val.set_fix("irqbalance", type="script", isauto=True)
checks.append( sys_val )

Config.default_category = "System"
Config.default_subcategory = "Disk"

sys_val = Check("Filesystem mount count for: %%val%%")
sys_val.to_run("tune2fs -l %%val%% |grep 'Maximum mount count' |awk '{print $NF}'")
sys_val.set_description("File system Mount Count option should be disabled")
sys_val.equal(-1)
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_MOUNTED")
sys_val.set_condition_id("Current UID")
sys_val.set_fix("cmd_wrapper tune2fs -c -1 -C -1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Filesystem check interval for: %%val%%")
sys_val.to_run("tune2fs -l %%val%% |grep 'Check interval:' |sed 's@(.*@@g' |awk '{print x$NF}'")
sys_val.set_description("File system Interval Check option should be disabled")
sys_val.equal(0)
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_MOUNTED")
sys_val.set_condition_id("Current UID")
sys_val.set_fix("cmd_wrapper tune2fs -i 0", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Filesystem: /var")
sys_val.to_run("grep -c ' /var ' /proc/mounts")
sys_val.equal(1, "important")
sys_val.set_description("/var should have its own file system")
sys_val.set_fix("Please dedicate a /var partition with enough free space")
checks.append( sys_val )

sys_val = Check("Free space for /boot")
sys_val.to_run("df -Pk /boot |awk '{if($1 ~ \"^/\"){printf $2}}'")
sys_val.greater_or_eq(1073741824, "recommended")
sys_val.set_description("/boot should be at least 1GB of space")
sys_val.set_fix("Please dedicate a /boot partition with enough free space")
checks.append( sys_val )

sys_val = Check("Free space in /var")
sys_val.to_run("df -Pk /var |awk '{if($1 ~ \"^/\"){printf $4}}'")
sys_val.greater_or_eq(CHECK_CONFIG.supervisor_space_var)
sys_val.set_fix("Please extend the /var/partition size to {0}".format(CHECK_CONFIG.supervisor_space_var))
checks.append( sys_val )

Config.default_category = "System"
Config.default_subcategory = "Security"

sys_val = Check("SELinux deactivation")
sys_val.to_run("type getenforce >/dev/null 2>&1 && getenforce || echo Disabled")
sys_val.set_description("SELinux has to be deactivated (CentOS & RHEL)")
sys_val.equal("Disabled")
sys_val.set_fix("selinux", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("AppArmor deactivation")
sys_val.to_run("test -f /etc/init.d/apparmor && /etc/init.d/apparmor status |awk '{printf $0; exit}' || echo unloaded")
sys_val.set_description("AppArmor have to be deactivated (Ubuntu)")
sys_val.regex("^((?! loaded\.).)+$")
sys_val.set_condition_id("Current UID")
sys_val.set_fix("cmd_wrapper /etc/init.d/apparmor stop && /etc/init.d/apparmor teardown && update-rc.d -f apparmor remove", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("IPTables modules")
sys_val.to_run("resources/scripts/iptables_mods.sh")
sys_val.equal("unloaded", "recommended")
sys_val.set_description("IPTables kernel modules should not be loaded")
checks.append( sys_val )

Config.default_category = "System"
Config.default_subcategory = "Environment"

sys_val = Check("NTP peering")
sys_val.to_run("ntpdc -c sysinfo 2>/dev/null |grep peer: |sed 's@.*:[ ]*@@'")
sys_val.set_description("NTP client should be synchronized with a NTP server")
sys_val.notequal("")
sys_val.set_fix("Please configure or deploy your own NTP server")
checks.append( sys_val )

sys_val = Check("Default shell for root")
sys_val.to_run("grep ^root: /etc/passwd |awk -F: '{print $7}'")
sys_val.equal("/bin/bash", "recommended")
sys_val.set_description("bash should be the default shell for root")
sys_val.set_fix("rootshell", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Current UID")
sys_val.to_run("os.geteuid()", type="python")
sys_val.equal(0)
sys_val.set_description("This script should be run as root, not all tests will be able to run for normal user")
sys_val.set_fix("You can use the --sudo argument to get root access", type="text")
checks.append( sys_val )

sys_val = Check("SAR logging")
sys_val.to_run("ls /var/log/sa/sa* /var/log/sysstat/sa* 2>/dev/null |wc -w")
sys_val.greater_or_eq(1, "recommended")
sys_val.set_description("SAR should be activated and have journalisation")
sys_val.set_fix("please enable the cron jobs for sadm")
checks.append( sys_val )

sys_val = Check("Localhost definition")
sys_val.to_run("sed 's/#.*//' /etc/hosts | grep -wic localhost", type="shell")
sys_val.equal("1", "recommended")
sys_val.set_description("localhost should be defined once in the /etc/hosts file")
sys_val.set_fix("please check that you have only one instance for localhost")
checks.append( sys_val )

#
# ULIMIT PARAMETERS
#

Config.default_category = "System"
Config.default_subcategory = "Limits"

sys_val = Check("Process's data segment")
sys_val.to_run("resources/scripts/ulimit.sh -d")
sys_val.equal(-1, "recommended")
sys_val.set_description("process's data segment should be unlimited (ulimit -d)")
sys_val.set_fix("ulimit data -1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max number of pending signals")
sys_val.to_run("resources/scripts/ulimit.sh -i")
sys_val.greater_or_eq("1031513", "recommended")
sys_val.set_description("Max number of pending signals should be at least '1031513' (ulimit -i)")
sys_val.set_fix("ulimit sigpending 1031513", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max locked memory size")
sys_val.to_run("resources/scripts/ulimit.sh -l")
sys_val.equal(64, "recommended")
sys_val.set_description("Max locked memory size should be set to '64' (ulimit -l)")
sys_val.set_fix("ulimit memlock 64", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Size of files written by the shell")
sys_val.to_run("resources/scripts/ulimit.sh -f")
sys_val.equal(-1, "recommended")
sys_val.set_description("Size of files written by the shell should be unlimited (ulimit -f)")
sys_val.set_fix("ulimit fsize -1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max RSS")
sys_val.to_run("resources/scripts/ulimit.sh -m")
sys_val.equal(-1, "recommended")
sys_val.set_description("Max RSS should be unlimited (ulimit -m)")
sys_val.set_fix("ulimit rss -1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max open files")
sys_val.to_run("resources/scripts/ulimit.sh -n")
sys_val.greater_or_eq(65535, "recommended")
sys_val.set_description("Max open files should be at least '65535' (ulimit -n)")
sys_val.set_fix("ulimit nofile 65535", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Pipe size in 512-byte blocks")
sys_val.to_run("resources/scripts/ulimit.sh -p")
sys_val.equal(8, "recommended")
sys_val.set_description("Pipe size should be set to '8' (ulimit -p) to match 4k window")
checks.append( sys_val )

sys_val = Check("Max number of bytes in POSIX msg queues")
sys_val.to_run("resources/scripts/ulimit.sh -q")
sys_val.equal(819200, "recommended")
sys_val.set_description("Max size of msg queues should be set to '819200' (ulimit -q)")
sys_val.set_fix("ulimit msgqueue 819200", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max stack size")
sys_val.to_run("resources/scripts/ulimit.sh -s")
sys_val.equal(10240, "recommended")
sys_val.set_description("Max stack size should be set to '10240' (ulimit -s)")
sys_val.set_fix("ulimit stack 10240", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max amount of CPU time")
sys_val.to_run("resources/scripts/ulimit.sh -t")
sys_val.equal(-1, "recommended")
sys_val.set_description("Max amount of CPU time should be unlimited (ulimit -t)")
sys_val.set_fix("ulimit cpu -1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max user processes")
sys_val.to_run("resources/scripts/ulimit.sh -u")
sys_val.greater_or_eq(1031513, "recommended")
sys_val.set_description("Max user processes should be set to '1031513' (ulimit -u)")
sys_val.set_fix("ulimit nproc 1031513", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Max virtual memory for shell")
sys_val.to_run("resources/scripts/ulimit.sh -v")
sys_val.equal(-1, "recommended")
sys_val.set_description("Max virtual memory for shell should be unlimited (ulimit -v)")
checks.append( sys_val )

sys_val = Check("Max number of file locks")
sys_val.to_run("resources/scripts/ulimit.sh -x")
sys_val.equal(-1, "recommended")
sys_val.set_description("Max number of file locks should be unlimited (ulimit -x)")
sys_val.set_fix("ulimit locks -1", type="script", isauto=True)
checks.append( sys_val )
#
# End ULIMIT PARAMETERS
#

Config.default_category = "System"
Config.default_subcategory = "Memory"

sys_val = Check("Check swappiness current value")
sys_val.to_run("sysctl -e -n vm.swappiness")
sys_val.equal(0, "recommended")
sys_val.lesser_or_eq(10, "important")
sys_val.set_description("swapping should be avoided, vm.swappiness value '0' is recommended, '10' is the maximum value")
sys_val.set_fix("sysctl vm.swappiness 0", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Number of semaphores: SEMMSL")
sys_val.to_run("sysctl -e -n kernel.sem |awk '{print $1}'")
sys_val.set_description("Minimum recommended semaphore value: 256")
sys_val.greater_or_eq("256", "important")
sys_val.greater_or_eq("512", "recommended")
sys_val.set_fix("sysctl 1 kernel.sem 512", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Number of semaphores: SEMMNS")
sys_val.to_run("sysctl -e -n kernel.sem |awk '{print $2}'")
sys_val.greater_or_eq("32000", "recommended")
sys_val.set_description("Maximum semaphore value in the system: 32000 (tuned for 500 streams)")
sys_val.set_fix("sysctl 2 kernel.sem 32000", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Number of semaphores: SEMOPM")
sys_val.to_run("sysctl -e -n kernel.sem |awk '{print $3}'")
sys_val.greater_or_eq("32", "recommended")
sys_val.set_description("Maximum number of operations for each semaphore call: 32")
sys_val.set_fix("sysctl 3 kernel.sem 32", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Number of semaphores: SEMMNI")
sys_val.to_run("sysctl -e -n kernel.sem |awk '{print $4}'")
sys_val.greater_or_eq("256", "recommended")
sys_val.set_description("Maximum number of semaphore sets in the entire system: 256")
sys_val.set_fix("sysctl 4 kernel.sem 256", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Panic on OOM")
sys_val.to_run("sysctl -e -n vm.panic_on_oom")
sys_val.equal(0)
sys_val.set_description("System should not reboot on OOM")
sys_val.set_fix("sysctl vm.panic_on_oom 0", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("keep \"not reserved\" memory space")
sys_val.to_run("sysctl -e -n vm.min_free_kbytes")
sys_val.greater_or_eq(2000000)
sys_val.set_description("At least 2GB of memory should not be reserved")
sys_val.set_fix("sysctl vm.min_free_kbytes 2000000", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Overcommit heuristic activation")
sys_val.to_run("sysctl -e -n vm.overcommit_memory")
sys_val.equal(0)
sys_val.set_description("Allow heuristic overcommit")
sys_val.set_fix("sysctl vm.overcommit_memory 0", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Cache pressure")
sys_val.to_run("sysctl -e -n vm.vfs_cache_pressure")
sys_val.lesser_or_eq(50, "recommended")
sys_val.set_description("Vfs cache pressure should be less than 50 to reduce swapping")
sys_val.set_fix("sysctl vm.vfs_cache_pressure 50", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Transparent hugepages")
sys_val.to_run("fgrep -c '[never]' /sys/kernel/mm/transparent_hugepage/enabled", type="shell")
sys_val.equal("1", "important")
sys_val.set_description("Transparent hugepages have to be disabled")
checks.append( sys_val )

sys_val = Check("Glibc version")
sys_val.to_run("resources/scripts/glibc.sh", type="shell")
sys_val.equal("OK", "important")
sys_val.set_description("Glibc minimum version required")
sys_val.set_fix("install glibc", type="script", isauto=True)
checks.append( sys_val )

Config.default_category = "System"
Config.default_subcategory = "Network"

sys_val = Check("TTL value")
sys_val.to_run("sysctl -e -n net.ipv4.ip_default_ttl", type="shell")
sys_val.equal(64)
sys_val.set_description("Default TTL value has to be '64'")
sys_val.set_fix("sysctl net.ipv4.ip_default_ttl 64", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("TCP_FIN time out")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_fin_timeout", type="shell")
sys_val.fail_if_not_in_range(10,30)
sys_val.set_description("tcp_fin_timeout should be set between 10-30")
sys_val.set_fix("sysctl net.ipv4.tcp_fin_timeout 10", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Number of incoming connections")
sys_val.to_run("sysctl -e -n net.core.somaxconn", type="shell")
sys_val.greater_or_eq(4096)
sys_val.set_description("Number of max socket connections, 4096 recommended")
sys_val.set_fix("sysctl net.core.somaxconn 4096", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Socket reuse")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_tw_reuse", type="shell")
sys_val.equal(1)
sys_val.set_description("Allow to reuse TIME-WAIT sockets for new connections")
sys_val.set_fix("sysctl net.ipv4.tcp_tw_reuse 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Sockets fast recycling")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_tw_recycle", type="shell")
sys_val.equal(1)
sys_val.set_description("Allow sockets to fast recycle")
sys_val.set_fix("sysctl net.ipv4.tcp_tw_recycle 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("TCP sends out keepalive")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_keepalive_time", type="shell")
sys_val.equal(1800)
sys_val.set_description("Send out a TCP keepalive each 30 minutes")
sys_val.set_fix("sysctl net.ipv4.tcp_keepalive_time 1800", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("TCP window scaling")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_window_scaling", type="shell")
sys_val.equal(1)
sys_val.set_description("Enable the window scaling")
sys_val.set_fix("sysctl net.ipv4.tcp_window_scaling 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("rmem_max")
sys_val.to_run("sysctl -e -n net.core.rmem_max", type="shell")
sys_val.set_description("Maximum Receive socket memory")
sys_val.equal(1677721600)
sys_val.set_fix("sysctl net.core.rmem_max 1677721600", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("wmem_max")
sys_val.to_run("sysctl -e -n net.core.wmem_max", type="shell")
sys_val.equal(1677721600)
sys_val.set_description("Maximum Send socket memory")
sys_val.set_fix("sysctl net.core.wmem_max 1677721600", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("net.core.netdev_max_backlog")
sys_val.to_run("sysctl -e -n net.core.netdev_max_backlog", type="shell")
sys_val.equal(3000)
sys_val.set_description("maximum network input buffer queue length")
sys_val.set_fix("sysctl net.core.netdev_max_backlog 3000", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_rmem")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_rmem", type="shell")
sys_val.regex("^4096[ \t]*174760[ \t]*16777216$")
sys_val.set_description("Minimum, Initial and Maximum size of a receive socketbuffer (bytes)")
sys_val.set_fix("sysctl net.ipv4.tcp_rmem '4096 174760 16777216'", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_wmem")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_wmem", type="shell")
sys_val.regex("^4096[ \t]*174760[ \t]*16777216$")
sys_val.set_description("Minimum, Initial and Maximum size of a send socketbuffer (bytes)")
sys_val.set_fix("sysctl net.ipv4.tcp_wmem '4096 174760 16777216'", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_moderate_rcvbuf")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_moderate_rcvbuf", type="shell")
sys_val.equal(1)
sys_val.set_description("Allow TCP performs receive buffer auto-tuning")
sys_val.set_fix("sysctl net.ipv4.tcp_moderate_rcvbuf 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("rmem_default")
sys_val.to_run("sysctl -e -n net.core.rmem_default", type="shell")
sys_val.equal(174760)
sys_val.set_description("Default size of a receive socketbuffer (bytes)")
sys_val.set_fix("sysctl net.core.rmem_default 174760", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("wmem_default")
sys_val.to_run("sysctl -e -n net.core.wmem_default", type="shell")
sys_val.equal(174760)
sys_val.set_description("Default size of a send socketbuffer (bytes)")
sys_val.set_fix("sysctl net.core.wmem_default 174760", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("optmem_max")
sys_val.to_run("sysctl -e -n net.core.optmem_max", type="shell")
sys_val.equal(524287)
sys_val.set_description("maximum amount of option memory buffers")
sys_val.set_fix("sysctl net.core.optmem_max 524287", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("lo.arp_filter")
sys_val.to_run("sysctl -e -n net.ipv4.conf.lo.arp_filter", type="shell")
sys_val.equal(1)
sys_val.set_description("Allows to have multiple network interfaces on the same subnet")
sys_val.set_fix("sysctl net.ipv4.conf.lo.arp_filter 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_dsack")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_dsack", type="shell")
sys_val.equal(1)
sys_val.set_description("Recommended '1'")
sys_val.set_fix("sysctl net.ipv4.tcp_dsack 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_sack")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_sack", type="shell")
sys_val.equal(1)
sys_val.set_description("Recommended '1'")
sys_val.set_fix("sysctl net.ipv4.tcp_sack 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_mem")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_mem", type="shell")
sys_val.regex("^1024000[ \t]*8738000[ \t]*1677721600$")
sys_val.set_description("Minimum, pressure and maximum TCP pages.")
sys_val.set_fix("sysctl net.ipv4.tcp_mem '1024000 8738000 1677721600'", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("ip_local_port_range")
sys_val.to_run("sysctl -e -n net.ipv4.ip_local_port_range", type="shell")
sys_val.regex("^20480[ \t]*65000$")
sys_val.set_description("Extend the default local port range between 20480 -> 65000")
sys_val.set_fix("sysctl net.ipv4.ip_local_port_range '20480 65000'", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_syncookies")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_syncookies", type="shell")
sys_val.equal("0")
sys_val.set_description("Should be set to '0' Please set it to '1' if the network is not protected. (SYN flood attack)")
sys_val.set_fix("sysctl net.ipv4.tcp_syncookies 0", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("tcp_timestamps")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_timestamps", type="shell")
sys_val.equal(1)
sys_val.set_description("Recommended '1' if 10Gbe interface, '0' if not present on the system")
sys_val.set_fix("sysctl net.ipv4.tcp_timestamps 1", type="script", isauto=True)
checks.append( sys_val )

#sys_val = Check("tcp_congestion_control")
#sys_val.to_run("sysctl -e -n net.ipv4.tcp_congestion_control", type="shell")
#sys_val.equal("reno")
#sys_val.set_description("The default, well-tested 'reno' algorithm is recommended")
#sys_val.set_fix("sysctl net.ipv4.tcp_congestion_control reno", type="script", isauto=True)
#checks.append( sys_val )

sys_val = Check("tcp_mtu_probing")
sys_val.to_run("sysctl -e -n net.ipv4.tcp_mtu_probing", type="shell")
sys_val.equal(1)
sys_val.set_description("tcp_mtu_probing should be set to '1' to help avoid MTU black holes")
sys_val.set_fix("sysctl net.ipv4.tcp_mtu_probing 1", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("conf.all.accept_redirects")
sys_val.to_run("sysctl -e -n net.ipv4.conf.all.accept_redirects", type="shell")
sys_val.equal(0, "recommended")
sys_val.set_description("Refuse network redirections")
sys_val.set_fix("sysctl net.ipv4.conf.all.accept_redirects 0", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("conf.all.send_redirects")
sys_val.to_run("sysctl -e -n net.ipv4.conf.all.send_redirects", type="shell")
sys_val.equal(0)
sys_val.set_description("Refuse network redirections")
sys_val.set_fix("sysctl net.ipv4.conf.all.send_redirects 0", type="script", isauto=True)
checks.append( sys_val )

Config.default_category = "System"
Config.default_subcategory = "Software"

sys_val = Check("Sysstat installation")
sys_val.to_run("is_installed('sysstat')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install sysstat", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Procps installation")
sys_val.to_run("is_installed('procps|procps-ng')", type="python")
sys_val.equal("True", "important")
sys_val.set_fix("install procps procps-ng", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Bc installation")
sys_val.to_run("is_installed('bc')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install bc", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Dstat installation")
sys_val.to_run("is_installed('dstat')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install dstat", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("E2fsprogs installation")
sys_val.to_run("is_installed('e2fsprogs')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install e2fsprogs", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Gdb installation")
sys_val.to_run("is_installed('gdb')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install gdb", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Htop installation")
sys_val.to_run("is_installed('htop')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install htop", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Iotop installation")
sys_val.to_run("is_installed('iotop')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install iotop", type="script", isauto=True)
checks.append( sys_val )

#sys_val = Check("Iozone installation")
#sys_val.to_run("is_installed('iozone3|iozone')", type="python")
#sys_val.equal("True", "recommended")
#sys_val.set_fix("install iozone3 iozone", type="script", isauto=True)
#checks.append( sys_val )

sys_val = Check("Iperf installation")
sys_val.to_run("is_installed('iperf')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install iperf iperf3", type="script", isauto=True)
checks.append( sys_val )

#sys_val = Check("Jnettop installation")
#sys_val.to_run("is_installed('jnettop')", type="python")
#sys_val.equal("True", "recommended")
#sys_val.set_fix("install jnettop jnettop", type="script", isauto=True)
#checks.append( sys_val )

sys_val = Check("Lsof installation")
sys_val.to_run("is_installed('lsof')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install lsof", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Mtr installation")
sys_val.to_run("is_installed('mtr')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install mtr", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Openssh-client installation")
sys_val.to_run("is_installed('openssh-client|openssh-clients')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install openssh-client openssh-clients", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Parted installation")
sys_val.to_run("is_installed('parted')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install parted", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Pciutils installation")
sys_val.to_run("is_installed('pciutils')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install pciutils", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Rsync installation")
sys_val.to_run("is_installed('rsync')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install rsync", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Screen installation")
sys_val.to_run("is_installed('screen')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install screen", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Smartmontools installation")
sys_val.to_run("is_installed('smartmontools')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install smartmontools", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Strace installation")
sys_val.to_run("is_installed('strace')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install strace", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Sudo installation")
sys_val.to_run("is_installed('sudo')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install sudo", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Tcpdump installation")
sys_val.to_run("is_installed('tcpdump')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install tcpdump", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Telnet installation")
sys_val.to_run("is_installed('telnet')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install telnet", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Traceroute installation")
sys_val.to_run("is_installed('traceroute')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install traceroute", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Vim installation")
sys_val.to_run("is_installed('vim-enhanced|vim')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install vim-enhanced vim", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Wget installation")
sys_val.to_run("is_installed('wget')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install wget", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Zip installation")
sys_val.to_run("is_installed('zip')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install zip", type="script", isauto=True)
checks.append( sys_val )

sys_val = Check("Hdparm installation")
sys_val.to_run("is_installed('hdparm')", type="python")
sys_val.equal("True", "recommended")
sys_val.set_fix("install hdparm", type="script", isauto=True)
checks.append( sys_val )

Config.default_category = "Network"
Config.default_subcategory = "status"

sys_val = Check("Speed for interface: %%val%%")
sys_val.to_run("ethtool %%val%% 2>/dev/null |grep 'Speed: ' |sed 's@[^0-9]@@g'", type="shell")
sys_val.greater_or_eq(10000, "recommended")
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_INET")
sys_val.set_description("Interfaces need to run at 10Gb/s")
checks.append( sys_val )

sys_val = Check("Duplex for interface: %%val%%")
sys_val.to_run("ethtool %%val%% 2>/dev/null |grep 'Duplex: ' |awk '{print $NF}'", type="shell")
sys_val.equal("Full")
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_INET")
sys_val.set_description("Network interface has to be in full duplex mode")
checks.append( sys_val )

sys_val = Check("RX buffer for interface: %%val%%")
sys_val.to_run("ethtool --show-ring %%val%% 2>/dev/null |awk '/^Current/,0{if($1~/RX:/){print $NF}}'", type="shell")
sys_val.equal(4096, "recommended")
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_PHYS_NICS")
sys_val.set_description("Network interface reception buffer should be set to 4096")
sys_val.set_fix("ethtool --set-ring %%val%% rx 4096")
checks.append( sys_val )

sys_val = Check("TX buffer for interface: %%val%%")
sys_val.to_run("ethtool --show-ring %%val%% 2>/dev/null |awk '/^Current/,0{if($1~/TX:/){print $NF}}'", type="shell")
sys_val.equal(4096, "recommended")
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_PHYS_NICS")
sys_val.set_description("Network interface transmission buffer should be set to 4096")
sys_val.set_fix("ethtool --set-ring %%val%% tx 4096")
checks.append( sys_val )

sys_val = Check("Generic segmentation offload for interface: %%val%%")
sys_val.to_run("ethtool --show-offload %%val%% 2>/dev/null |awk '{if($1~/generic-segmentation-offload:/){print $2}}'", type="shell")
sys_val.equal("on")
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_PHYS_NICS")
sys_val.set_description("Network interface segmentation offload should be 'on'")
sys_val.set_fix("ethtool --offload %%val%% gso on")
checks.append( sys_val )

sys_val = Check("Large receive offload for interface: %%val%%")
sys_val.to_run("ethtool --show-offload %%val%% 2>/dev/null |awk '{if($1~/large-receive-offload:/){print $2}}'", type="shell")
sys_val.equal("on")
sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_PHYS_NICS")
sys_val.set_description("Network interface large receive offload should be 'on'")
sys_val.set_fix("ethtool --offload %%val%% lro on")
checks.append( sys_val )

Config.default_category = "Network"
Config.default_subcategory = "Ports"

sys_val = Check("Elasticsearch TCP ports")
sys_val.to_run("ss -tlnp |grep -w LISTEN |egrep -c ':9[23]00'", type="shell")
sys_val.equal("0", "important")
sys_val.set_description("TCP ports 9200,9300 should be free for elasticsearch")
sys_val.set_fix("Please stop and disable any service using the 9200 and 9300 tcp ports")
checks.append( sys_val )

#
# To check manually
#
Config.default_category = "TO BE IMPLEMENTED"
Config.default_subcategory = "PLEASE CHECK IT MANUALLY"

sys_val = Check("Disk perf test (write)")
sys_val.to_run("echo false")
sys_val.set_description("Performance test: write")
checks.append( sys_val )

sys_val = Check("Disk perf test (read)")
sys_val.to_run("echo false")
sys_val.set_description("Performance test: read")
checks.append( sys_val )

sys_val = Check("Raid controller cache size")
sys_val.to_run("echo false")
sys_val.set_description("Raid controllers should have at least 1GB of cache")
checks.append( sys_val )

sys_val = Check("Number of 10Gb/s network Interface")
sys_val.to_run("echo false")
sys_val.equal("true", "unknown")
sys_val.set_description("Number of 10Gb/s network Interface")
sys_val.set_fix("You are missing network inteerfaces")
checks.append( sys_val )

sys_val = Check("10Gb/s network Interface placement")
sys_val.to_run("echo false")
sys_val.equal("true", "unknown")
sys_val.set_description("10Gb/s network Interfaces should be on a PCIex8 slot")
sys_val.set_fix("Move network cards to a PCIex8 slot at minimum")
checks.append( sys_val )

sys_val = Check("System disks raid configuration")
sys_val.to_run("echo false")
sys_val.equal("true", "unknown")
sys_val.set_description("System disks should be mirrored")
checks.append( sys_val )

sys_val = Check("network perf between servers")
sys_val.to_run("echo false")
sys_val.equal("true", "unknown")
sys_val.set_description("All servers should have enough bandwith between them")
checks.append( sys_val )

#
# End To check manualluy
#

# Disabled tests

#sys_val = Check("test", category="test")
#sys_val.to_run("resources/scripts/test_ssh.sh " + setting.ipaddresses, type="shell")
#sys_val.equal("true", "unknown")
#checks.append( sys_val )


#sys_val = Check("Linux distribution", category="System", subcategory="OS")
#sys_val.to_run("platform.linux_distribution()[0]+platform.linux_distribution()[1]", type="python")
#sys_val.regex("Ubuntu1[24].04|CentOS6.[05-9]|CentOS Linux7|Red Hat Enterprise Linux Server6.[05-9]|Red Hat Enterprise Linux Server7")
#sys_val.set_description("Linux distribution supported: RHEL & CentOS 6.5/7, Ubuntu 12.04 & 14.04")
#checks.append( sys_val )

#sys_val = Check("Core dump creation", category="System", subcategory="Limits")
#sys_val.to_run("resources/scripts/ulimit.sh -c")
#sys_val.equal(0, "recommended")
#sys_val.set_description("core dumps should be avoided (ulimit -c)")
#checks.append( sys_val )

#sys_val = Check("Current GID", category="System", subcategory="User")
#sys_val.to_run("os.getgid()", type="python")
#sys_val.equal(0)
#sys_val.set_description("This script should be run at root, not all tests will be able to run for normal user")
#checks.append( sys_val )

#sys_val = Check("NSCD service", category="System", subcategory="Daemons")
#sys_val.to_run("ps -ef |grep -v grep |grep -cw nscd")
#sys_val.equal(0)
#sys_val.set_description("Nscd have to be deactivated")
#checks.append( sys_val )

#sys_val = Check("Salt-minion installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('salt-minion')", type="python")
#sys_val.equal("True", "important")
#checks.append( sys_val )

#sys_val = Check("Bmon installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('bmon')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Bind-utils installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('bind9utils|bind-utils')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Bonnie++-utils installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('bonnie++')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Createrepo installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('createrepo')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Lynx installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('lynx')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Man installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('man|man-db')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Net-snmp installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('snmpd|net-snmp')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Net-snmp-perl installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('net-snmp-perl|libsnmp-perl')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Net-snmp-utils installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('net-snmp-utils')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Ngrep installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('ngrep')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Nsca-client installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('nsca-client')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("perl-config-tiny installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('perl-Config-Tiny|libconfig-tiny-perl')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Perl-Crypt-Rijndael installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('perl-Crypt-Rijndael|libcrypt-rijndael-perl')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Net-snmp-perl installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('net-snmp-perl|libnet-snmp-perl')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Rpmdevtools installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('rpmdevtools')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Yum-utils installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('yum-utils')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("LVM2 installation", category="System", subcategory="Software")
#sys_val.to_run("is_installed('lvm2')", type="python")
#sys_val.equal("True", "recommended")
#checks.append( sys_val )

#sys_val = Check("Auto-negotiation for interface: %%val%%", category="Network", subcategory="status")
#sys_val.to_run("ethtool %%val%% 2>/dev/null |awk '{if($1~/Auto-negotiation:/){print $2}}'", type="shell")
#sys_val.equal("off", "recommended")
#sys_val.set_duplicate_cmd("resources/scripts/list_generation.sh V_PHYS_NICS")
#sys_val.set_description("Network interface autoneg should be 'off'")
#checks.append( sys_val )
