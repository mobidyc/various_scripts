#!/bin/bash
#
# vim:tabstop=3:syntax=sh:foldmethod=marker

yes_or_no () {
	PROMPT=$1     # message to display

	while true
	do
		/bin/echo -ne "$PROMPT [Y/N]? \c"
		read -r ANSWER
		ANSWER=$(echo "$ANSWER" |tr "[A-Z]" "[a-z]")

		case $ANSWER
		in
			yes|y|oui|o)	return 0 ;;
			non|no|n)		return 1 ;;
		esac
	done
}

is_ip_configured() {
	# return true if the IP in argument is configured on the system
	checktheip=$1

	for ip in $(ip addr |grep -vw "lo" |grep -w inet |awk '{print $2}' |cut -d'/' -f1)
	do
		if [ "$checktheip" = "$ip" ] ; then
			return 0
		fi
	done
	return 1
}

check_hostname() {
	# return true if the hostname in argument is configured on the system
	# We want to check for the short hostname only, case insensitive
	hn=$(echo $1 |awk -F. '{print tolower($1)}')
	if [ "$hn" = "$(hostname |awk -F. '{print tolower($1)}')" ] ; then
		return 0
	fi
	return 1
}

