#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

from __future__ import print_function

import os
import sys
import errno
import unicodedata
import json

from resources.Config import Config


class log():

    def init(self):
        try:
            os.makedirs(Config.dirlog)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(Config.dirlog):
                pass
            else:
                raise

        try:
            Config.fd_html = open(Config.html_output, 'a')
        except IOError, e:
            print(e, end="")
            sys.exit(1)

    def open_logs(self):
        try:
            Config.fd_file = open(Config.file_output, 'a')
            Config.fd_obj = open(Config.obj_output, 'a')
        except IOError, e:
            print(e, end="")
            sys.exit(1)

        if Config.DEBUG > 0:
            print ("FD: " + Config.file_output)
            print ("FD: " + Config.obj_output)

    def __exit__(self):
        self.close(Config.fd_html)
        if Config.fd_file:
            self.close(Config.fd_file)
            self.close(Config.fd_obj)

    def close(self, fd):
        try:
            fd.flush()
            fd.close()
        except:
            pass

    def std(self, txt, remove_last=False):
        if remove_last:
            back = "\b \b" * Config.last_stdout_msg_len
            print(back, end="")

        try:
            print(txt, end="")
        except UnicodeEncodeError:
            txt = unicodedata.normalize('NFKD', txt).encode('ascii', 'ignore')
            print(txt, end="")
        finally:
            try:
                Config.last_stdout_msg_len = len(txt)
            except TypeError:
                pass
            sys.stdout.flush()

    def debug(self, txt):
        if Config.DEBUG > 0:
            self.std("DEBUG: " + txt)

    def fd_write(self, txt, terminal, fd, remove_last=False):
        try:
            fd.write(txt)
        except UnicodeEncodeError:
            fd.write(unicodedata.normalize('NFKD', txt).encode('ascii', 'ignore'))
        except IOError, e:
            print(e, end="")
        finally:
            fd.flush()

    def file(self, txt, terminal=True, remove_last=False):
        self.fd_write(txt, terminal, Config.fd_file, remove_last)

    def html(self, txt):
        self.fd_write(txt + "\n", False, Config.fd_html)

    def jsonlog(self, txt):
        from resources.common import byteify
        try:
            if Config.DEBUG > 0:
                json.dump(byteify(txt), Config.fd_obj, sort_keys=True, indent=4, ensure_ascii=False)
            else:
                json.dump(byteify(txt), Config.fd_obj, ensure_ascii=False)
        finally:
            Config.fd_obj.flush()
