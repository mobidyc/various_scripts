#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

import getopt
import sys
import re

try:
    import json
except ImportError:
    import simplejson as json

from resources.Config import Config
from resources.log import log as logging
from resources.common import read_file


def parse_args(*argv):
    shrt_args = "hnctjrvJ:O:C:S:f:sd:D:qQTRwo:p"
    long_args = ["help", "no-run", "display-command", "display-test",
                 "json", "run", "verbose", "jsfile=", "objfile=",
                 "category=", "sub-category=", "file=", "show",
                 "discard-category", "discard-subcategory", "quiet",
                 "quick", "trace", "sudo", "with-color",
                 "output=", "process-logs"
                 ]

    try:
        opts, args = getopt.getopt(sys.argv[1:], shrt_args, long_args)
    except getopt.GetoptError:
        return False

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            return False

        elif opt in ("-n", "--no-run"):
            Config.allow_running = False
        elif opt in ("-c", "--display-command"):
            Config.allow_running = False
            Config.norun_args.append("cmd")
        elif opt in ("-s", "--show"):
            Config.allow_running = False
            Config.norun_args.append("show")
        elif opt in ("-t", "--display-test"):
            Config.allow_running = False
            Config.norun_args.append("diff")
        elif opt in ("-j", "--json"):
            Config.allow_running = False
            Config.display_json_tests = True
        elif opt in ("-p", "--process-logs"):
            Config.obj_only = True
        elif opt in ("-T", "--trace"):
            Config.DEBUG += 1

        elif opt in ("-v", "--verbose"):
            set_remote_arg(opt, arg)
            Config.verbosity += 1
        elif opt in ("-w", "--with-color"):
            Config.nocolor = False
            set_remote_arg(opt, arg)
        elif opt in ("-R", "--sudo"):
            set_remote_arg(opt, arg)
            Config.sudo = "sudo "
        elif opt in ("-q", "--quiet"):
            set_remote_arg(opt, arg)
            Config.only_failed = True
        elif opt in ("-Q", "--quick"):
            Config.quick_display = True
        elif opt in ("-J", "--jsfile"):
            set_remote_arg(opt, arg)
            Config.JSFILES.append(arg)
        elif opt in ("-O", "--objfile"):
            set_remote_arg(opt, arg)
            Config.OBJFILES.append(arg)
        elif opt in ("-d", "--discard-category"):
            set_remote_arg(opt, arg)
            Config.notcat.append(arg)
        elif opt in ("-D", "--discard-subcategory"):
            set_remote_arg(opt, arg)
            Config.notsub.append(arg)
        elif opt in ("-C", "--category"):
            set_remote_arg(opt, arg)
            Config.cat.append(arg)
        elif opt in ("-S", "--sub-category"):
            set_remote_arg(opt, arg)
            Config.sub.append(arg)
        elif opt in ("-o", "--output"):
            set_remote_arg(opt, arg)
            Config.dirlog = arg
            Config.remote_dirlog = arg
        elif opt in ("-f", "--file"):
            Config.servers_json = open_jsonfile(arg)


def set_remote_arg(opt, arg):
    Config.args_for_remote += " " + opt + " " + arg


def open_jsonfile(file):
    log = logging()
    not_in_zip_file = True
    whole_lines = read_file(file, not_in_zip_file)
    lines = re.sub('//.*', '', whole_lines)

    try:
        read_json = json.loads(lines)
    except ValueError as e:
        log.std(e)
        sys.exit(1)

    return read_json
