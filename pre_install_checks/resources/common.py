#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

import os
import sys
import zipfile
import platform
import socket
import fcntl
import struct
import array
import subprocess
import multiprocessing

try:
    import json
except ImportError:
    import simplejson as json

from resources.Config import Config
from resources.app import pkg_resources
from log import log as logging


class local_infos():
    def hostname(self):
        # We don't want the FQDN
        return platform.node().split('.')[0]

    def ifaces(self):
        def format_ip(addr):
            return "{0}.{1}.{2}.{3}".format(ord(addr[0]), ord(addr[1]), ord(addr[2]), ord(addr[3]))

        max_possible = 128
        bytes = max_possible * 32
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        names = array.array('B', '\0' * bytes)
        outbytes = struct.unpack('iL', fcntl.ioctl(
            s.fileno(),
            0x8912,  # SIOCGIFCONF
            struct.pack('iL', bytes, names.buffer_info()[0])
        ))[0]

        namestr = names.tostring()
        lst = []
        for i in range(0, outbytes, 40):
            name = namestr[i:i + 16].split('\0', 1)[0]
            ipad = namestr[i + 20:i + 24]
            lst.append((name, format_ip(ipad)))
        return lst

    def drive_list(self):
        # lsblk -P -d -o KNAME,ROTA,SIZE,TYPE
        out = []
        try:
            p = subprocess.Popen('lsblk -n -d -o KNAME,ROTA,SIZE,TYPE', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            for line in iter(p.stdout.readline, ''):
                (kname, rota, size, type) = line.split()
                out.append('KNAME="{0}" ROTA="{1}" SIZE="{2}" TYPE="{3}"'.format(kname, rota, size, type))
        except subprocess.CalledProcessError as e:
            return e.returncode
        except:
            return False

        return out

    def mem_size(self):
        mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
        mem_gib = mem_bytes / (1024.**3)

        return str(round(mem_gib, 2)) + "G"

    def uname(self):
        return platform.platform()

    def cpu(self):
        out = {'arch': platform.architecture(), 'count': str(multiprocessing.cpu_count())}

        return out


def read_file(filename, force_fs=False):
    log = logging()
    lines = ""

    # Check if file is local or not
    relp = os.path.relpath(filename)

    # if outside the script or in root tree
    """
    if relp[0:3] == "../" or "/" not in relp:
        try:
            f = open(filename, 'r')
        except IOError, e:
            log.std(e)
            sys.exit(2)
        else:
            lines = f.read()
            f.close()
    else:
        d = os.path.dirname(relp).replace('/', '.')
        lines = pkg_resources.resource_string(d, os.path.basename(relp))
    """
    if zipfile.is_zipfile(sys.argv[0]) and not force_fs:
        d = os.path.dirname(relp).replace('/', '.')
        lines = pkg_resources.resource_string(d, os.path.basename(relp))
    else:
        try:
            f = open(filename, 'r')
        except IOError, e:
            log.std(e)
            sys.exit(2)

        lines = f.read()
        f.close()

    return lines


def extract_script(command):
    log = logging()
    """ take a command name with all its arguments,
    if needed, extract the script then return the
    modified command
    """

    if command is None:
        return command

    # Test if we are a zipped script or not
    try:
        archive = zipfile.ZipFile(sys.argv[0], 'r')
    except zipfile.BadZipfile:
        return command

    cmd_list = command.split()
    for arg in range(len(cmd_list)):
        prg = cmd_list[arg]
        if prg.startswith(Config.script_dir):
            data = archive.read(prg)
            newprg = prg.replace(Config.script_dir, Config.mytmp)
            try:
                f = open(newprg, 'w')
            except IOError as e:
                log.std(e)

            f.write(data)
            f.flush()
            f.close()
            cmd_list[arg] = newprg

            os.chmod(newprg, 0700)

    str = " "
    newcommand = str.join(cmd_list)
    return newcommand


def importJsonFile(json_file):
    log = logging()

    try:
        input_file = file(json_file, "r")
        my_json = json.loads(input_file.read().decode("utf-8-sig"))
    except ValueError as e:
        log.std(e)
    return my_json

    """
    lines = unicode(read_file(json_file), 'latin-1')
    try:
        my_json = json.loads(lines)
    except ValueError as e:
        log.std(e)
    return my_json
    """


def byteify(input):
    if isinstance(input, dict):
        return dict([(byteify(key), byteify(value)) for (key, value) in input.iteritems()])
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input
