#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

import subprocess
import re
import sys

try:
    import json
except ImportError:
    import simplejson as json

from Check import Check
from log import log as logging
import color
from common import read_file
from common import extract_script
from resources.Config import Config
import CHECK_CONFIG


class Checks(Check):
    def __init__(self, *args, **kwargs):
        """ Arguments:
        - obj=<list of check() objects>
        - json_file=<file.json> (content has to be json encoded Check objects)
        - json=<json encoded Check objects>
        """
        self.check_list = []
        self.log = logging()
        self.count_w = 0
        self.count_e = 0
        self.count_g = 0
        self.count_u = 0
        self.myobj = {"checks": []}

        if "obj" in kwargs:
            self.importObjList(kwargs["obj"])

        if "obj_file" in kwargs:
            self.importObjFile(kwargs["obj_file"])

        if "testdef_file" in kwargs:
            self.importTestDefFile(kwargs["testdef_file"])

        if "json_file" in kwargs:
            self.importJsonFile(kwargs["json_file"])

        if "json" in kwargs:
            self.importJson(kwargs["json"])

    def getCategories(self, *args, **kwargs):
        """ return a filtered dictionnary list with all categories and subcategories """
        my_objects = self.output_checks()
        category = {}

        for key in my_objects.keys():
            cat = my_objects[key]["category"]
            sub = my_objects[key]["subcategory"]

            # Remove the unwanted cat and sub
            go_next_key = False
            if "notcat" in kwargs:
                for n in kwargs["notcat"]:
                    if cat == n:
                        go_next_key = True
                        break
                if go_next_key:
                    continue

            if "notsub" in kwargs:
                for n in kwargs["notsub"]:
                    if sub == n:
                        go_next_key = True
                        break
                if go_next_key:
                    continue

            # List only the wanted cat and sub, need to revert the boolean usage
            if "cat" in kwargs and kwargs["cat"]:
                for n in kwargs["cat"]:
                    if cat == n:
                        go_next_key = True
                        break
                if go_next_key:
                    pass
                else:
                    continue

            if "sub" in kwargs and kwargs["sub"]:
                for n in kwargs["sub"]:
                    if sub == n:
                        go_next_key = True
                        break
                if go_next_key:
                    pass
                else:
                    continue

            if cat not in category:
                category[cat] = {}
            category[cat].update({sub: {}})

        return category

    def importObjFile(self, obj_file):
        checks = []
        lines = read_file(obj_file)
        exec(lines)
        self.importObjList(checks)

    def importTestDefFile(self, def_file):
        checks = []
        with open(def_file, "r") as ins:
            new_check = Check('temp')  # dummy check skeleton, to be populated

            for line in ins:
                # Skip comment lines
                if line[0] == "#":
                    continue
                # line starting with a dash delimits a new check, so we have to
                # import previous registered check
                if line[0] == '-' and new_check.ID != 'temp':
                    checks.append(new_check)
                    new_check = Check('temp')

                # if no check to import, go to the next line
                elif line[0] == '-' or line[0] == ' ':
                    continue
                # skip empty lines
                elif not line.strip():
                    continue
                # else we create a new check
                else:
                    action, value = line.split(":", 1)

                    if action == "id":
                        new_check.set_id(str(value))
                    if action == 'cat':
                        new_check.set_category(str(value))
                    if action == 'sub':
                        new_check.set_subcategory(str(value))
                    if action == 'test':
                        new_check.command_to_run = value.rstrip()
                    if action == 'test_type':
                        new_check.command_type = str(value.rstrip())
                    if action == 'desc':
                        new_check.set_description(str(value))
                    if action == 'duplicate_cmd':
                        new_check.set_duplicate_cmd(value.rstrip())
                    if action == 'equal':
                        new_check.equal(value.rstrip())
                # End if
            # End for
        # End with

        # handle the end of file
        if new_check.ID != 'temp':
            checks.append(new_check)
        self.importObjList(checks)

    def importObjList(self, obj_list):
        """ import object list """

        """ import the actual checks if any, so we can supersede them """
        if len(self.check_list) > 0:
            my_objects = self.output_checks()
        else:
            my_objects = {}

        for i in range(len(obj_list)):
            w = obj_list[i].ID
            dict = {obj_list[i].ID: {}}
            dict[w].update({
                "category": obj_list[i].category,
                "subcategory": obj_list[i].subcategory,
                "command_type": obj_list[i].command_type,
                "command": obj_list[i].command_to_run,
                "current_value": obj_list[i].current_value,
                "duplicate_cmd": obj_list[i].duplicate_cmd,
                "condition_id": obj_list[i].condition_id,
                "descr": obj_list[i].descr,
                "fix": obj_list[i].fix,
                "fixtype": obj_list[i].fixtype,
                "fixisauto": obj_list[i].fixisauto,
                "tests": []
            })

            dict[w].update({"category": obj_list[i].category})
            dict[w].update({"subcategory": obj_list[i].subcategory})
            dict[w].update({"command_type": obj_list[i].command_type})
            dict[w].update({"command": obj_list[i].command_to_run})
            dict[w].update({"current_value": obj_list[i].current_value})
            dict[w].update({"duplicate_cmd": obj_list[i].duplicate_cmd})
            dict[w].update({"condition_id": obj_list[i].condition_id})
            dict[w].update({"descr": obj_list[i].descr})

            dict[w].update({"tests": []})
            for t in range(len(obj_list[i].tests)):
                dict[w]["tests"].append(obj_list[i].tests[t])

            my_objects.update(dict)

        # convert json in object
        self.importJson(json.dumps(my_objects))

    def importJsonFile(self, json_file):
        lines = read_file(json_file)
        try:
            self.importJson(lines)
        except ValueError as e:
            self.log.std(e)
            sys.exit(1)

    def importJson(self, js):
        """ import json code and convert it into an object list """

        """ import the actual checks if any, so we can supersede them """
        if len(self.check_list) > 0:
            my_objects = self.output_checks()
            self.check_list = []
        else:
            my_objects = {}

        my_json = json.loads(js)
        for key in my_json.keys():
            my_objects.update({key: my_json[key]})

        for key in my_objects.keys():
            obj = Check(key)
            obj.set_category(my_objects[key]["category"])
            obj.set_subcategory(my_objects[key]["subcategory"])
            obj.set_currentVal(my_objects[key]["current_value"])
            obj.to_run(my_objects[key]["command"], type=my_objects[key]["command_type"])
            obj.duplicate_cmd = my_objects[key]["duplicate_cmd"]
            obj.condition_id = my_objects[key]["condition_id"]
            obj.descr = my_objects[key]["descr"]
            obj.tests = my_objects[key]["tests"]
            obj.fix = my_objects[key]["fix"]
            obj.fixtype = my_objects[key]["fixtype"]
            obj.fixisauto = my_objects[key]["fixisauto"]
            self.check_list.append(obj)

    def getChecks(self, *args, **kwargs):
        """ return the checks descriptions plus informations, arguments can be:
            - cmd to display the command used to retrieve a value
            - diff to display information about the tests
        """
        category = self.getCategories(**kwargs)
        l = range(len(self.check_list))
        r_unwanted = re.compile("[\n]")

        if "show" in args:
            for cat in category.keys():
                self.log.std("\n[%s]" % cat)
                for sub in category[cat].keys():
                    self.log.std("\n   [%s]" % sub)
            return

        for cat in category.keys():
            self.log.std("\nCategory %s:" % cat)

            for sub in category[cat].keys():
                self.log.std(u"\n└──Sub-Category %s:" % sub)

                for iter in l:
                    if cat == self.check_list[iter].category and sub == self.check_list[iter].subcategory:
                        self.log.std(u"\n   └──ID: ──> %s <──" % self.check_list[iter].ID)
                        if self.check_list[iter].condition_id:
                            self.log.std(u"\n            Will be run if: %s" % self.check_list[iter].condition_id)

                        if "cmd" in args and self.check_list[iter].command_to_run is not None:
                            self.log.std(u"\n          %s Command is: %s" % (self.check_list[iter].command_type, r_unwanted.sub("\\\\n", self.check_list[iter].command_to_run)))
                            if self.check_list[iter].duplicate_cmd:
                                self.log.std("\n                      Iter: --> %s <--" % self.check_list[iter].duplicate_cmd)
                            self.log.std(u"\n          Current value is: %s" % (self.check_list[iter].current_value))

                        if "diff" in args:
                            for t in range(len(self.check_list[iter].tests)):
                                self.log.std(u"\n          test: %s" % self.check_list[iter].tests[t])

    def output_checks(self):
        """ output all the checks stored in the self.check_list list in json format """

        out = {}
        for i in range(len(self.check_list)):
            w = self.check_list[i].ID
            dict = {self.check_list[i].ID: {}}

            dict[w].update({
                "category": self.check_list[i].category,
                "subcategory": self.check_list[i].subcategory,
                "command_type": self.check_list[i].command_type,
                "command": self.check_list[i].command_to_run,
                "current_value": self.check_list[i].current_value,
                "duplicate_cmd": self.check_list[i].duplicate_cmd,
                "condition_id": self.check_list[i].condition_id,
                "descr": self.check_list[i].descr,
                "fix": self.check_list[i].fix,
                "fixtype": self.check_list[i].fixtype,
                "fixisauto": self.check_list[i].fixisauto,
                "tests": []
            })

            for t in range(len(self.check_list[i].tests)):
                dict[w]["tests"].append(self.check_list[i].tests[t])

            out.update(dict)

        return out

    def val_of(self, ID):
        if self.run_id(ID):
            for iter in range(len(self.check_list)):
                if ID in self.check_list[iter].ID:
                    return self.check_list[iter].current_value
        return False

    def create_dups(self):
        myobjs = []
        for i in range(len(self.check_list)):
            if self.check_list[i].duplicate_cmd:
                command = extract_script(self.check_list[i].duplicate_cmd)
                try:
                    """ Get the iterators """
                    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                    values = p.stdout.read()
                    retval = p.wait()
                    del retval
                except:
                    self.log.std("ERROR running the shell command: \n" + self.check_list[i].command_to_run, remove_last=True)

                vals = re.sub('\s{2,}', ' ', values.strip().replace('\n', ' '))

                if len(vals) == 0:
                    continue

                list_vals = vals.strip().split(" ")

                v_ID = self.check_list[i].ID
                v_cat = self.check_list[i].category
                v_sub = self.check_list[i].subcategory
                v_cmd_type = self.check_list[i].command_type
                v_cmd = self.check_list[i].command_to_run
                v_descr = self.check_list[i].descr
                v_tests = self.check_list[i].tests
                v_cond = self.check_list[i].condition_id
                v_fix = self.check_list[i].fix
                v_fixtype = self.check_list[i].fixtype
                v_fixisauto = self.check_list[i].fixisauto

                for d in list_vals:
                    new_obj = Check(v_ID.replace('%%val%%', ' ' + d + ' '), category=v_cat, subcategory=v_sub)
                    new_obj.to_run(v_cmd.replace('%%val%%', ' ' + d + ' '), type=v_cmd_type)
                    new_obj.set_description(v_descr)
                    new_obj.set_condition_id(v_cond)
                    new_obj.tests = v_tests
                    new_obj.fix = v_fix
                    new_obj.fixtype = v_fixtype
                    new_obj.fixisauto = v_fixisauto
                    myobjs.append(new_obj)

        self.importObjList(myobjs)

    def run_id(self, ID):
        """ Recursively run the command of an ID """
        for iter in range(len(self.check_list)):
            if ID in self.check_list[iter].ID:
                return self.check_list[iter].run_the_command()

        return False

    def run_condition(self, index):
        """ Recursively run a condition and display it
            needs the self.check_list 'index' where the condition_id can be found
        """
        condition_id = self.check_list[index].condition_id
        ret = False

        for iter in range(len(self.check_list)):
            if condition_id in self.check_list[iter].ID and self.check_list[iter].can_be_run:
                # Recursivity starts here
                if self.check_list[iter].condition_id:
                    self.run_condition(iter)
                # End recursivity

        for iter in range(len(self.check_list)):
            if condition_id in self.check_list[iter].ID:
                self.run_id(self.check_list[iter].ID)
                self.check_list[iter].so_what()

                st = self.check_list[iter].status
                if st == "important" or st == "unknown":
                    self.check_list[index].can_be_run = False
                    self.check_list[index].status = "cannot"
                    if st == "important":
                        self.count_e += 1
                    else:
                        self.count_u += 1
                    break
                else:
                    if st == "good":
                        self.count_g += 1
                        ret = True
                    elif st == "recommended":
                        self.count_w += 1
                    else:
                        self.count_u += 1

        return ret

    def run_all(self, **kwargs):
        category = self.getCategories(**kwargs)

        for cat in category.keys():
            self.log.file("\n" + color.cat(cat) + ":")

            for sub in category[cat].keys():
                self.log.file("\n " + color.sub(sub) + ":\n")

                for iter in range(len(self.check_list)):
                    if cat == self.check_list[iter].category and sub == self.check_list[iter].subcategory:

                        if self.check_list[iter].condition_id:
                            self.run_condition(iter)

                        if not self.check_list[iter].can_be_run:
                            pass
                        if not self.run_id(self.check_list[iter].ID):
                            self.log.file("\n      '%s': ERROR" % self.check_list[iter].ID)
                        else:
                            for test in self.check_list[iter].tests:
                                for key in range(len(test)):
                                    if re.match("^self\.val_of\(", str(test[key])):
                                        test[key] = eval(str(test[key]))
                            self.check_list[iter].so_what()

                        st = self.check_list[iter].status
                        if st == "important":
                            self.count_e += 1
                        elif st == "recommended":
                            self.count_w += 1
                        elif st == "good":
                            self.count_g += 1
                        else:
                            if self.check_list[iter].duplicate_cmd:
                                continue
                            if not self.check_list[iter].can_be_run:
                                self.myobj["checks"].append(self.check_list[iter].check_obj)
                                continue
                            self.count_u += 1
                        self.myobj["checks"].append(self.check_list[iter].check_obj)

        self.log.std("End of checks", remove_last=True)

    def display_counter(self):
        self.log.file("\n")

        passed = "PASS: {0}".format(self.count_g)
        recomm = "RECOMMENDED: {0}".format(self.count_w)
        warnin = "IMPORTANT: {0}".format(self.count_e)
        unknow = "UNKNOWN: {0}".format(self.count_u)

        output = "\n-{0} -{1} -{2} -{3}\n".format(passed, recomm, warnin, unknow)
        self.log.file(output)
        self.log.std(output)
