#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

from resources.Config import Config
from resources.common import importJsonFile
from resources.log import log as logging
from resources.common import read_file
from resources.beautify_bash import BeautifyBash
import shutil
import sys
import os


class autofix(object):
    def __init__(self, jsonfile):
        """ Import a json file containing the result of pre install checks
        also create a script containing all the necessary fixes """

        self.log = logging()

        try:
            self.obj = importJsonFile(jsonfile)
        except IOError as e:
            print "An IOError occurred. {0}".format(e)
            sys.exit(1)

        self.output_dir = os.path.dirname(jsonfile)
        self.to_script()

    def to_script(self):
        """ generates the script to fix elements, per server """

        """ logic is:
            - set all fix functions in the header of the script
            - for each node, generate the correct list of fixes
            - create a condition basedon hostname + ip addresses to be sure we
            deploy fixes on the good hardware
        """

        self.auto_script = read_file(Config.fix_template)
        self.auto_function = {}

        """ Used to detect at the end of the script, if we ran or not """
        self.auto_message = "\nRAN=1\n"

        for node in self.obj["nodes"]:
            self.get_node_fixes(node)

        for fixfile in self.auto_function:
            f = os.path.join(Config.fix_funcdir, fixfile)
            self.auto_script += "\n{0}".format(read_file(f))

        self.auto_message += """\n[ "$RAN" = "1" ] && echo "You cannot fix this server, It has not be analyzed"\n"""

        script = os.path.join(self.output_dir, Config.fix_script)
        bash_indent, error = BeautifyBash().beautify_string(self.auto_message)
        try:
            shutil.copy("/dev/null", script)
            fd = open(script, 'a')
            fd.write("{0} {1}".format(self.auto_script, bash_indent))
            fd.close()
        except IOError, e:
            print e
            sys.exit(1)

        end_msg = """

If you want to set most of the recommended parameters,
please run the following script on each servers
    # {0}
For instance, if you want to run it easily from SSH, the following command should be enough:
    # for i in server1 server2 ;do
      scp {0} $i:/tmp/
      ssh -ttt $i "bash /tmp/{1}"
    done

        """.format(script, Config.fix_script)
        self.log.std(end_msg)

    def get_node_fixes(self, node):
        hostname = node.get('hostname')
        checks = node.get('checks')

        self.auto_message += "GOOD_TO_RUN=true\n"
        self.auto_message += "check_hostname {0} || GOOD_TO_RUN=false\n".format(hostname)

        for nic in node.get('nic'):
            self.auto_message += "is_ip_configured {0} || GOOD_TO_RUN=false\n".format(nic['ip'])

        self.auto_message += """if [ "$GOOD_TO_RUN" = "true" ] ; then\n"""
        self.auto_message += "\nRAN=0\n"

        """
        1. we display fixes which need confirmation
        2. we display auto fixes
        3. we display manual fixes
        4. possibly display checks with no fix
        """
        fix_auto = self.get_auto(node.get('checks'), "script", True)
        fix_halfauto = self.get_auto(node.get('checks'), "script")
        fix_mano = self.get_auto(node.get('checks'), "text")
        fix_nofix = self.get_auto(node.get('checks'), None)

        """ selectively add all required functions in the header """
        for elem in fix_halfauto + fix_auto:
            self.auto_function.update({elem['fix'].split(' ', 1)[0]: {}})

        # 1. we display fixes which need confirmation
        for elem in fix_halfauto:
            self.auto_message += """
            yes_or_no "Do you want to fix '{0}'"
            [ "$?" = "0" ] && fix_{1}
            """.format(elem['id'], elem['fix'])

        # 2. we display auto fixes and group pkg installs
        if fix_auto:
            fix_auto_install = ""
            for elem in fix_auto:
                if "install" in elem['id']:
                    f = elem['fix'][8:]
                    fix_auto_install += " {0}".format(f)
                self.auto_message += "echo -- {0}\n".format(elem['id'])
            self.auto_message += """
            yes_or_no "You are about to automatically set the above list of fixes, Do you agree"
            if [ "$?" = "0" ] ; then
            """
            install_written = False
            for elem in fix_auto:
                if "install" in elem['id']:
                    if not install_written:
                        self.auto_message += "fix_install {0}\n".format(fix_auto_install)
                        install_written = True
                else:
                    self.auto_message += "fix_{0}\n".format(elem['fix'])
            self.auto_message += "fi\n"

        # 3. we display manual fixes
        if fix_mano:
            self.auto_message += "\necho Please fix the following list of recommendation:\n"
            for elem in fix_mano:
                self.auto_message += "echo ' - {0}: {1}'\n".format(elem['id'], elem['fix'])

        # 4. possibly display checks with no fix
        if fix_nofix:
            self.auto_message += "echo Please fix the following list of recommendation:\n"
            for e in range(len(fix_nofix)):
                self.auto_message += "echo -- '- {0}: NO FIX DEFINED YET'\n".format(fix_nofix[e]['id'])

        self.auto_message += "fi\n\n"

    # get auto, semi auto or text fixes
    def get_auto(self, obj, type, auto=False):
        """ Return a list of elements with a known fix

            get a list of Checks
            Filter on fixisauto and fixtype values
        """
        elems = []
        for elem in obj:
            """ We exclude unknown and already good checks """
            if elem.get('status') == "unknown":
                continue
            if elem.get('status') == "good":
                continue
            if elem.get('status') == "cannot":
                continue

            if elem.get('fix') is not None:
                if elem.get('fixtype') == type and elem.get('fixisauto') == auto:
                    elems.append(elem)
            elif type is None:
                elems.append(elem)

        return elems


