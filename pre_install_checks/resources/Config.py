#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

__author__       = "Cedrick GAILLARD"
__copyright__    = "Copyright 2015, Scality"
__maintainer__   = "Cedrick GAILLARD"
__email__        = "cedrick.gaillard@scality.com"
__status__       = "dev"

import os
import tempfile
import zipfile
import sys


class _Config(object):
    def __init__(self):
        self.basedir = os.path.abspath(os.getcwd())
        self.root_folder = os.path.basename(self.basedir)

        """ Generic tests and templates"""
        self.tests_folder        = "Generics"
        self.html_header  = "resources/tpl/global_header.html"
        self.html_footer  = "resources/tpl/global_footer.html"
        self.fix_template = "resources/tpl/fix_script.sh"
        self.fix_funcdir  = "resources/scripts/fixes"
        self.fix_script   = "apply_fixes.sh"

        """ Logging """
        self.dirlog        = os.path.join(self.basedir, "scality-results")
        self.remote_dirlog = "scality-results"
        self.file_log      = "pre-check-install.log"
        self.html_log = "global.html"
        self.obj_log       = "pre-check-install.json"

        # For pexpect
        self.ssh_newkey_question = 'Are you sure you want to continue connecting'
        self.ssh_success         = 'Connection established successfully'

        """ These cat & sub will be run forced """
        self.category_to_run_alone = ["TO BE IMPLEMENTED"]
        self.subcateg_to_run_alone = ["PLEASE CHECK IT MANUALLY"]

        """ Default settings """
        self.verbosity           = 0
        self.allow_running       = True
        self.display_json_tests  = False
        self.args_for_remote     = ""
        self.passwds             = {}
        self.cat                 = []
        self.sub                 = []
        self.last_pwd            = None
        self.last_stdout_msg_len = 0
        self.DEBUG               = 0
        self.script_dir          = "resources/scripts"
        self.sudo                = ""
        self.obj_only            = False
        self.ipaddresses         = " "
        self.role                = "generic"
        self.nocolor             = True
        self.customer            = ""
        self.default_category    = "UnCategorized"
        self.default_subcategory = "UnCategorized"

        self.norun_args          = []
        self.quick_display       = False
        self.only_failed         = False
        self.JSFILES             = []
        self.OBJFILES            = []
        self.servers_json        = {}

    @property
    def generic_test_file(self):
        return os.path.join(self.tests_folder, "included.py")

    @property
    def generic_def_file(self):
        return os.path.join(self.tests_folder, "included.def")

    @property
    def remote_root_folder(self):
        if zipfile.is_zipfile(sys.argv[0]):
            return "/tmp"
        else:
            return os.path.join("/tmp", self.root_folder)

    @property
    def included_tests(self):
        return os.path.join(self.basedir, self.generic_test_file)

    @property
    def included_defs(self):
        return os.path.join(self.basedir, self.generic_def_file)

    @property
    def mytmp(self):
        return tempfile.mkdtemp(prefix=self.root_folder)

    @property
    def file_output(self):
        return '%s' % (os.path.join(self.dirlog, self.file_log))

    @property
    def html_output(self):
        return os.path.join(self.dirlog, self.html_log)

    @property
    def obj_output(self):
        return os.path.join(self.dirlog, self.obj_log)

Config = _Config()
