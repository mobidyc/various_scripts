#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

import sys
import time
from operator import itemgetter

from resources.Config import Config
from resources.log import log as logging
from resources.common import read_file
from resources.common import importJsonFile
from resources import version
from resources.autofix import autofix


__author__ = "Cedrick GAILLARD"
__copyright__ = "Copyright 2015, Scality"
__maintainer__ = "Cedrick GAILLARD"
__email__ = "cedrick.gaillard@scality.com"
__status__ = "dev"


class obj_export(object):
    def __init__(self, jsonfile):
        self.log = logging()

        try:
            self.obj = importJsonFile(jsonfile)
        except IOError, e:
            print e
            sys.exit(1)

        self.to_html()

        autofix(jsonfile)

    def to_html(self):
        # Used in switch/case statement to improve message
        compare_msg = {
            "equal": {
                "true": "is equal to",
                "false": "has to be equal to"
            },
            "regex": {
                "true": "matches the regex",
                "false": "has to match the regex"
            },
            "notequal": {
                "true": "is different than",
                "false": "has to be different than"
            },
            "lesser_or_eq": {
                "true": "is lesser (or equal) than",
                "false": "has to be lesser (or equal) than"
            },
            "greater_or_eq": {
                "true": "is greater (or equal) than",
                "false": "has to be greater (or equal) than"
            },
            "fail_if_not_in_range": {
                "true": "is in the range",
                "false": "has to be in the range"
            }
        }

        # Alias
        s = self.log.html

        # Write the html HEADERS
        s(read_file(Config.html_header))

        s('<div id="version">Version: {v}</div>'.format(v=version.__version__))
        s('<div id="date">Last run: {d}</div>'.format(d=time.strftime("%c")))

        s('<div class="roles">')
        s('<div id="cust">Customer: {cust}</div>'.format(cust=self.get_customer()))
        s('</div>')

        # We want to group by role
        for uniq_role, count in self.get_roles():
            s('<div class="roles">')

            diffs = self.get_differences_status(self.obj, uniq_role)
            if diffs:
                s('<div class="vert_list">ROLE: {role} (<a href="#">{diff} differences </a>)</div>'.format(role=uniq_role, diff=len(diffs)))
                s('<div class="diffs">')
                for key in diffs:
                    s('  <div class="vert_head">{cle}</div>'.format(cle=key))
                    s('  <ul class="tcell vert_list">')
                    s('<li></li>')
                    for srv in diffs[key]:
                        s('    <li><div class="{diff}">{serv}</div></li>'.format(diff=diffs[key][srv], serv=srv))
                    s('  </ul>')
                    s('<div></div>')
                s('</div>')
            else:
                if count > 1:
                    s('<div class="vert_list">ROLE: {role} - all servers are identic</div>'.format(role=uniq_role))
                else:
                    s('<div class="vert_list">ROLE: {role}</div>'.format(role=uniq_role))

            # comparison if we have more than one server in the role
            if count > 1:
                compare = self.get_hardware_diffs(uniq_role)

                if compare["uname"] == "BAD":
                    s('</br><div class="vert_list">Operating system is not the same on all servers</div>')
                if compare["cpu_arch"] == "BAD":
                    s('</br><div class="vert_list">Bad CPU architecture detected</div>')
                if compare["cpu_cnt"] == "BAD":
                    s('</br><div class="vert_list">number of CPU are not the same on all servers</div>')
                if compare["memory"] == "BAD":
                    s('</br><div class="vert_list">Amount of memory is not the same on all servers</div>')
                if compare["net"] == "BAD":
                    s('</br><div class="vert_list">Network Interfaces are not the same on all servers</div>')
                if compare["hdds"] == "BAD":
                    s('</br><div class="vert_list">Disks are not the same on all servers</div>')

            for node in self.obj["nodes"]:
                role = node.get('role')
                if uniq_role != role:
                    continue

                hostname = node.get('hostname')
                uname = node.get("os").get('uname')
                checks = node.get('checks')
                memory = node.get('memory').get('size')
                cpu_arch = node.get('cpu').get('arch')
                cpu_count = node.get('cpu').get('count')

                ssds = []
                hdds = []
                # for disk in node.iter('disk'):
                for disk in node.get('disk'):
                    if disk.get('rota') == '0':
                        ssds.append(disk.get('drives'))
                    else:
                        hdds.append(disk.get('drives'))
                ssd_count = sorted([(i, ssds.count(i)) for i in set(ssds)])
                hdd_count = sorted([(i, hdds.count(i)) for i in set(hdds)])

                # Start writing the HTML
                s('<div class="resuming">')

                s('  <div class="naming"><div class="vert_head">Hostname:</div><ul class="tcell vert_list">')
                s('    <li><div class="hostname d_cont">{host}</div></li>'.format(host=hostname))
                s('    <li><div class="uname">{uname}</div></li>'.format(uname=uname))
                s('  </ul></div>')

                s('  <div class="statuses"><div class="vert_head">Status:</div><ul class="vert_list">')
                s('    <li><div class="st_all"><a href="" class="status_info">all: {count}</a></div></li>'.format(count=self.get_statuses(checks, True)))
                for (st, cnt) in self.get_statuses(checks):
                    s('    <li><div class="{status}"><a href="#" class="status_info">{status}: {number}</a></div></li>'.format(status=st, number=cnt))
                s('    </ul></div>')

                s('  <div class="cpu"><div class="vert_head">CPU:</div><ul class="vert_list">')
                s('      <li>{nbr} x {arch}</li>'.format(nbr=cpu_count, arch=cpu_arch))
                s('  </ul></div>')

                s('  <div class="memory"><div class="vert_head">Memory:</div><ul class="vert_list">')
                s('    <li>{mem}</li>'.format(mem=memory))
                s('  </ul></div>')

                s('  <div class="nics">')
                s('    <div class="vert_head">Network:</div>')
                s('    <ul class="vert_list">')
                for nic in node.get('nic'):
                    s('      <li>{nicname} {nicip}</li>'.format(nicname=nic.get("name"), nicip=nic.get('ip')))
                s('    </ul>')
                s('  </div>')

                s('  <div class="disks">')
                if len(ssd_count) > 0:
                    s('    <div class="vert_head">SSD:</div><ul class="vert_list">')
                    for (size, count) in iter(ssd_count):
                        s('    <li>{count} x {size}</li>'.format(count=count, size=size))
                    s('    </ul>')

                    s('  <div class="clear"></div>')

                if len(hdd_count) > 0:
                    s('    <div class="vert_head">HDD:</div>')
                    s('    <ul class="vert_list">')
                    for (size, count) in iter(hdd_count):
                        s('    <li>{count} x {size}</li>'.format(count=count, size=size))
                    s('    </ul>')
                s('  </div>')

                # list all existing categories
                cats = self.list_categories(node)

                s("<div class='cats'>")
                for cat in cats:
                    s('	<div class="gcat">')
                    s("		<div class='cat'>{cat}</div>".format(cat=cat))
                    checks_by_cat = self.find_elem_tag(node, 'checks', 'category', cat)

                    # list all subcategories
                    subs = self.list_subcategories(checks_by_cat)
                    s("		<div class='subs'>")

                    for sub in subs:
                        s("			<div class='sub'>{0}</div>".format(sub))
                        checks_by_sub = self.find_elem_tag(checks_by_cat, None, 'subcategory', sub)

                        for check in checks_by_sub:
                            try:
                                st = check.get('status')
                                id = check.get('id')
                                can_run = check.get('can_run')
                                comm = check.get('command')
                                cmd = comm.get('cmd')
                                cmd_val = comm.get('val')
                                descr = check.get('description')
                                todo = check.get('todo')
                            except:
                                pass
                            if not st:
                                st = "unknown"

                            s("			<div class='check sel_{st} b_{st}'>".format(st=st))
                            s("				<div class='status'>")
                            s("					<div class='st {st}'>{st}</div>".format(st=st))
                            s("					<div class='id'>{id}</div>".format(id=id))
                            s("				</div>")

                            if can_run == "False":
                                depe = check.find('condition').get('name')
                                s("				<div class='row'>")
                                s("					<div class='st cannot'>DEPENDS ON</div>")
                                s("					<div class='id'>{d}</div>".format(d=depe))
                                s("				</div>")

                            if cmd:
                                s("				<div class='cmd'>")
                                s("					<div class='st comm'>COMMAND</div>")
                                s("					<div class='pre'>{cmd}</div>".format(cmd=cmd))
                                s("				</div>")

                            if descr:
                                s("				<div class='row'>")
                                s("					<div class='st d_cont'>Description</div>")
                                s("					<div class='descr'>{d}</div>".format(d=descr))
                                s("				</div>")

                            s("				<div class='test'>")

                            # to display the correct check message
                            msg_st = "true" if st == "good" else "false"

                            for test in check.get("tests"):
                                system_value = "<div class='sysval'> {val} </div>".format(val=cmd_val)
                                wanted_value = "<div class='tstval'> {val} </div>".format(val=test.get("val"))

                                msgone = "<div class='msg1'>Value on the system is: &#91;</div>  {system}  <div class='msg1'>&#93;</div>".format(system=system_value)
                                if test.get("name") in compare_msg:
                                    msgtwo = "<div class='msg2'> {name}: </div>".format(name=compare_msg[test.get("name")][msg_st])
                                else:
                                    msgtwo = "<div class='msg2'> {name}: </div>".format(name=test.get("name"))
                                msgthree = "<div class='msg2'>&#91;</div>  {val}  <div class='msg2'>&#93;</div>".format(val=wanted_value)

                                s("<div></div><div class='st unknown'>test</div>")
                                s("{a} {b} {c}".format(a=msgone, b=msgtwo, c=msgthree))
                            s("				</div>")
                            s("			</div><!--END check-->")
                    s('		</div><!--End class subs-->')
                    s('	</div><!--End class gcat-->')
                s('</div><!-- END cats -->')
                s('</div><!-- END resuming -->')
            s('</div><!-- END roles -->')

        # Write the html FOOTERS
        self.log.html(read_file(Config.html_footer))

    def find_elem_tag(self, obj, elem, tagname, tag):
        elems = []
        if elem is None:
            for check in obj:
                if check.get(tagname) == tag:
                    elems.append(check)
        else:
            for check in obj.get(elem):
                if check.get(tagname) == tag:
                    elems.append(check)
        return elems

    def list_categories(self, node):
        c = []
        for check in node.get("checks"):
            c.append(check.get('category'))

        all = list(set(c))
        # little orderization for the categories
        for up in Config.category_to_run_alone:
            if up in all:
                all.remove(up)
                all.insert(0, up)

        return all

    def list_subcategories(self, obj):
        c = []
        for check in obj:
            c.append(check.get('subcategory'))
        return list(set(c))

    def get_roles(self):
        roles = []
        for node in self.obj["nodes"]:
            roles.append(node.get('role'))
        return sorted([(i, roles.count(i)) for i in set(roles)], key=itemgetter(1))

    def get_differences_status(self, myobj, role):
        for uniq_role, count in self.get_roles():
            if count == 1:
                continue
            if uniq_role != role:
                continue

            node_temp = {"checks": {}}
            for node in myobj.get("nodes"):
                if uniq_role != node.get('role'):
                    continue

                hostname = node.get('hostname')
                checks = node.get('checks')

                for check in checks:
                    id = check.get('id')
                    st = check.get('status')
                    serv_status = {"srv": hostname, "status": st}

                    if id not in node_temp.get('checks'):
                        node_temp.get('checks').update({id: []})

                    node_temp.get('checks')[id].append(serv_status)

            to_display = {}
            for check, host_list in node_temp.get("checks").items():
                st = {}
                st_counts = []
                for host_st in host_list:
                    st.update({host_st.get('srv'): host_st.get('status')})
                    st_counts.append(host_st.get('status'))

                if len(set(st_counts)) > 1:
                    to_display.update({check: st})

            return to_display

    def get_hardware_diffs(self, role):
        compare = {
            "uname": None,
            "cpu_arch": None,
            "cpu_cnt": None,
            "memory": None,
            "hdds": None,
            "net": None,
        }
        for node in self.obj.get("nodes"):
            if role != node.get('role'):
                continue
            uname = node.get("os").get('uname')
            memory = node.get('memory').get('size')
            cpu_arch = node.get('cpu').get('arch')
            cpu_count = node.get('cpu').get('count')
            nics = []
            for nic in node.get("nic"):
                nics.append(nic.get("name"))
            hdds = []
            for disk in node.get('disk'):
                hdds.append(disk.get('drives'))

            if compare.get("uname") is None:
                compare["uname"] = uname
            if compare.get("cpu_arch") is None:
                compare["cpu_arch"] = cpu_arch
            if compare.get("cpu_cnt") is None:
                compare["cpu_cnt"] = cpu_count
            if compare.get("memory") is None:
                compare["memory"] = memory
            if compare.get("hdds") is None:
                compare["hdds"] = hdds
            if compare.get("net") is None:
                compare["net"] = nics

            if compare["uname"] != uname:
                compare["uname"] = "BAD"
            if compare["cpu_arch"] != cpu_arch:
                compare["cpu_arch"] = "BAD"
            if compare["cpu_cnt"] != cpu_count:
                compare["cpu_cnt"] = "BAD"
            if compare["memory"] != memory:
                compare["memory"] = "BAD"
            if sorted(compare["hdds"]) != sorted(hdds):
                compare["hdds"] = "BAD"
            if sorted(compare["net"]) != sorted(nics):
                compare["net"] = "BAD"

        return compare

    def get_customer(self):
        for node in self.obj.get("nodes"):
            try:
                cust = node.get('customer')
                if len(cust) > 0:
                    return cust
                    break
            except:
                return "Anonymous"

    def get_statuses(self, checks, count=False):
        check_status = []
        for i in range(len(checks)):
            check_status.append(checks[i].get('status'))
        if count:
            return len(check_status)
        if len(check_status) > 0:
            return sorted([(i, check_status.count(i)) for i in set(check_status)])
