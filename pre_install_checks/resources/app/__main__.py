#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

import pkg_resources


def main():
    print(pkg_resources.resource_string('resources', 'inFrance.txt'))

if __name__ == '__main__':
    main()
