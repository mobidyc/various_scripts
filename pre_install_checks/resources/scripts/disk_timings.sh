#!/bin/bash

# Need to have 2 arguments
# first is --ssd or --hdd to know on which disk type we want to perform timing
# second is -t or -T and will be the argument used with hdparm

[ "$(type hdparm >/dev/null 2>&1 ; echo $?)" -ne "0" ] && echo "hdparm not found" && exit 1

function timing_device() {
	[ -z "$3" ] && echo 0 && return 1

	V_type=$1
	V_arg=$2
	shift 2

	# if not at least two disks, exit.
	[ "$#" -le "1" ] && {
		echo "0"
		exit 0
	}

	cp /dev/null /tmp/${V_type}_timings.log
	# run the test 3 times
	pids=
	for disk in ${*}
	do
		# Subshell for some kind of optimisation
		(
		cp /dev/null /tmp/${V_type}_timings_${disk}${V_arg}.log
		for count in 1 2 3
		do
			hdparm -q $V_arg /dev/$disk >> /tmp/${V_type}_timings_${disk}${V_arg}.log
		done

		# We keep only the lower value to avoid false positives
		cut -d= -f2 /tmp/${V_type}_timings_${disk}${V_arg}.log |awk -v dev=$disk 'BEGIN {
			large=10000000000
		} {
			if( $1 < large ) { large = $1 }
		} END {
			print dev, large
		}' >> /tmp/${V_type}_timings.log
		) &
		pids="${pids} $!"
	done

	while [ "$(echo ${pids} |wc -w)" -ne "0" ]
	do
		for pid in $pids
		do
			if [ -n "$(ps --no-headers -p $pid)" ]
				then
					continue
			else
				pids=$(echo "${pids}" |tr " " "\n" |grep -v "^${pid}$" |tr "\n" " ")
			fi
		done
		sleep 10
	done

	awk 'BEGIN {
		small=1000000000000
		smalldev=""
		large=1
		largedev=""
	} {
		if( $2 > large ) { large = $2 ; largedev = $1}
		if( $2 < small ) { small = $2 ; smalldev = $1}
	} END {
		p = 100 - (small / large * 100)
		printf("smallest: %s %.0fMb/s, largest: %s %.0fMb/s, difference: %.0f%%", small, smalldev, large, largedev, p)
	}' /tmp/${V_type}_timings.log
}

[ -z "$2" ] && exit 1

# List disks
all_disks=$(lsblk -o KNAME,TYPE -dn |awk '{if($2 ~ /disk/){print $1}}')

[ -z "$all_disks" ] && exit 2

all_unused_disks=
for device in $all_disks
do
	blockdev --rereadpt /dev/$device 2>/dev/null

	# if /sys/blocl/sdX/*/dev file exists, there is partitions
	slice=$(ls -l /sys/block/${device}/*/dev 2>/dev/null)

	# go next if a slice exists
	if [ -n "$slice" ] ; then
		continue
	else
		# Double check in case the whole disk is used
		is_mntd=$(df | awk -v disk=/dev/$device '{if($1 == disk) {print $1}}')
		if [ -n "$is_mntd" ] ; then
			continue
		else
			# triple check to see if superblock exists
			if [ "$UID" = "0" -a "$(type tune2fs >/dev/null 2>&1 ; echo $?)" = "0" ] ; then
				[ "$(tune2fs -l /dev/$device >/dev/null 2>&1 ; echo $?)" -ne "0" ] && {
					all_unused_disks="$all_unused_disks $device"
				}
			fi
		fi
	fi
done

[ -z "$all_unused_disks" ] && exit 3

unused_ssd_disks=
unused_spn_disks=
for device in $all_unused_disks
do
	if [ "$(< /sys/block/${device}/queue/rotational)" = "0" ]
		then
			unused_ssd_disks="$unused_ssd_disks $device"
		else
			unused_spn_disks="$unused_spn_disks $device"
	fi
done

case "$1"
in
	--ssd) timing_device ${1##*--} $2 $unused_ssd_disks
	;;
	--hdd) timing_device ${1##*--} $2 $unused_spn_disks
	;;
esac

