#!/bin/bash

# Need to have 1 argument
#	 --ssd or --hdd or --count
LIST=
[ "$1" == "list" ] && LIST=true && shift 1

[ -z "$1" ] && exit 1

# List disks
all_disks=$(lsblk -o KNAME,TYPE -dn |awk '{if($2 ~ /disk/){print $1}}')

[ -z "$all_disks" ] && echo 0 && exit 1

all_unused_disks=
for device in $all_disks
do
	blockdev --rereadpt /dev/$device 2>/dev/null

	# if /sys/block/sdX/*/dev file exists, there is partitions
	slice=$(ls -l /sys/block/${device}/*/dev 2>/dev/null)

	# go next if a slice exists
	if [ -n "$slice" ] ; then
		continue
	else
		# Double check in case the whole disk is used
		is_mntd=$(df | awk -v disk=/dev/$device '{if($1 == disk) {print $1}}')
		if [ -n "$is_mntd" ] ; then
			continue
		else
			# triple check to see if superblock exists
			if [ "$UID" = "0" -a "$(type tune2fs >/dev/null 2>&1 ; echo $?)" = "0" ] ; then
				[ "$(tune2fs -l /dev/$device >/dev/null 2>&1 ; echo $?)" -ne "0" ] && {
					all_unused_disks="$all_unused_disks $device"
				}
			fi
		fi
	fi
done

[ -z "$all_unused_disks" ] && echo 0 && exit 1

unused_ssd_disks=
unused_spn_disks=
for device in $all_unused_disks
do
	if [ "$(< /sys/block/${device}/queue/rotational)" = "0" ]
		then
			unused_ssd_disks="$unused_ssd_disks $device"
		else
			unused_spn_disks="$unused_spn_disks $device"
	fi
done

if [ -n "$LIST" ] ; then
	case "$1"
	in
		--ssd) echo $unused_ssd_disks
		;;
		--hdd) echo $unused_spn_disks
		;;
	esac
else
	case "$1"
	in
		--ssd) echo $unused_ssd_disks |wc -w
		;;
		--hdd) echo $unused_spn_disks |wc -w
		;;
		--count) echo $unused_ssd_disks $unused_spn_disks |wc -w
		;;
	esac
fi

