#!/bin/bash

for serv in ${*}
do
	ssh -q -o StrictHostKeyChecking=no $serv "echo 0 >/dev/null"
	if [ x$?x != x0x ] ; then
		echo "ssh cnx failed on $serv"
		exit 1
	fi

	crossed=$(ssh $serv "for serv in ${*}; do
      ssh -q -o StrictHostKeyChecking=no \$serv \"echo 0 >/dev/null\";
      if [ x\$?x != x0x ] ; then echo \$serv ; exit 1; fi ;
   done")
	if [ x$?x != x0x ] ; then
		echo "ssh cnx failed from $crossed to $serv"
		exit 1
	fi
done

echo "pass"
