#!/bin/bash

number=$(dmidecode -t chassis 2>&1 |grep 'Number Of Power' |awk '{print $NF}')

[ -z "$number" ] && echo "0" && exit 0

# If not a number, return zero
[ -n "$(echo $number |sed 's/[0-9]//')" ] && echo "0" && exit 0

echo $number
