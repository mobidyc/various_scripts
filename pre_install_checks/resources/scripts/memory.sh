#!/bin/bash

page_size=$(getconf PAGE_SIZE 2>/dev/null)
page_size=${page_size:-4096}

mem_bytes=$(awk '/MemTotal:/ { printf "%0.f",$2 * 1024}' /proc/meminfo)
shmmax=$(echo "$mem_bytes * 0.70" | bc | cut -f 1 -d '.')
shmall=$(expr $mem_bytes / $page_size)
max_orphan=$(echo "$mem_bytes * 0.10 / 65536" | bc | cut -f 1 -d '.')
file_max=$(echo "$mem_bytes / 4194304 * 256" | bc | cut -f 1 -d '.')
max_tw=$(($file_max*2))
min_free=$(echo "($mem_bytes / 1024) * 0.01" | bc | cut -f 1 -d '.')
mem_gb=$(awk '/MemTotal:/ { printf "%0.f",$2 / 1024 / 1024}' /proc/meminfo)

# minimum tcp_rmem size: 1 page, usually 4K
# default tcp_rmem size: 1024 pages, usually 1Mo
# max tcp_rmem size: usually 4M
tcp_min_rmem=$page_size
tcp_def_rmem=$((1024 * $page_size))
tcp_max_rmem=$(echo "$tcp_def_rmem * $page_size / 1024" | bc | cut -f 1 -d '.')

# tcp_wmem may be larger than tcp_rmem
# In Scality system there is usually less write than receive.
tcp_min_wmem=$page_size
tcp_def_wmem=$((2048 * $page_size))
tcp_max_wmem=$(echo "$tcp_def_wmem * $page_size / 1024" | bc | cut -f 1 -d '.')

case $1
in
	"mem_gb") echo $mem_gb ;;
	"max_orphan") echo $max_orphan ;;
	"max_twbuckets") echo $max_tw ;;
	"rmem_min") echo $tcp_min_rmem ;;
	"rmem_def") echo $tcp_def_rmem ;;
	"rmem_max") echo $tcp_max_rmem ;;
	"rmem") echo "$tcp_min_rmem $tcp_def_rmem $tcp_max_rmem" ;;
	"wmem_min") echo $tcp_min_wmem ;;
	"wmem_def") echo $tcp_def_wmem ;;
	"wmem_max") echo $tcp_max_wmem ;;
	"wmem") echo "$tcp_min_wmem $tcp_def_wmem $tcp_max_wmem" ;;
	"min_free") echo $min_free ;;
	"filemax") echo $file_max ;;
esac
