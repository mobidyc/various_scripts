#!/bin/bash

# Only for CentOS|RedHat
isrhel=$(grep -c "Red Hat" /proc/version)
[ "$isrhel" -eq "0" ] && echo "OK" && exit 0

VERSION_ID=""
[ -f "/etc/os-release" ] && . /etc/os-release

# python -c "import platform;print(platform.platform())"
# Linux-2.6.32-573.18.1.el6.x86_64-x86_64-with-centos-6.7-Final
# Linux-3.10.0-229.el7.x86_64-x86_64-with-centos-7.1.1503-Core
# Linux-2.6.32-573.el6.x86_64-x86_64-with-redhat-6.7-Santiago
# Linux-3.10.0-327.el7.x86_64-x86_64-with-redhat-7.2-Maipo
if [ -z "$VERSION_ID" ] ; then
	platform=$(python -c "import platform;print(platform.platform())")
	VERSION_ID="$(echo $platform |grep -o 'with-.*' |cut -d- -f3 |cut -d. -f1)"
fi
[ -z "$VERSION_ID" ] && echo "Undefined Distribution" && exit 1

# glibc version
ver=$(rpm -q --queryformat "%{version}.%{release}\n" glibc |sed 's/el[0-9]_//g')
[ -z "$ver" ] && echo "Undefined version" && exit 1
ver1=$(echo $ver |cut --output-delimiter="" -d. -f1,2)
ver2=$(echo $ver |cut -d. -f3)
ver3=$(echo $ver |cut -d. -f4)
ver4=$(echo $ver |cut -d. -f5)

case "$VERSION_ID"
in
	"6") # 2.12.1.192
			[ "$ver1" -ge "212" -a "$ver2" -ge "1" -a "$ver3" -ge "192" ] || {
				echo "2.12.1.192 minimum" 
				exit 1
			}
			echo "OK"
			exit 0
			;;
	"7") # 2.17.106.2.4
			[ "$ver1" -ge "217" -a "$ver2" -ge "106" -a "$ver3" -ge "2" ] || {
				echo "2.17.106.2.4 minimum" 
				exit 1
			}
			if [ "$ver1.$ver2.$ver3" = "217.106.2" ] ; then
				[ "$ver4" -ge "4" ] || {
					echo "2.12.1.192 minimum" 
					exit 1
				}
			fi
			echo "OK"
			exit 0
			;;
esac

echo "Something went wrong"
exit 1

