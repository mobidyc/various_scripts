#!/bin/bash

[ -z "$1" ] && exit 1

PATH=$PATH:/sbin:/usr/sbin
export PATH

f=setup_lists.conf

if [ -f "$f" ]; then
	. $f

	[ -n "$list" ] && {
		echo $list
		exit 0
	}
fi

list=$(eval echo "\$$1")

[ -n "$list" ] && {
	eval echo $list
	exit 0
}

case $1
in
	"V_MOUNTED") grep ^/ /proc/mounts |awk '{if($3 ~ /ext/){print $1}}'
	;;
	"V_INET") ip -oneline -family inet addr show label '[^l]*' |cut -d: -f2 |awk '{print $1}'
	;;
	"V_PHYS_NICS") ip -oneline -family link addr show label '[^lb]*' |awk -F: '{print $2}'
	;;
esac


