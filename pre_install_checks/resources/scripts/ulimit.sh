#!/bin/bash

# exit 3 if no argument
# exit 2, echo "-2" if ulimit pbl
# echo "-1" for unlimited

test -z "$1" && exit 3

v_key=$1
v_val=$(ulimit -S $v_key)
rval=$?

test "$rval" -gt "0" && {
	echo -- "-2"
	exit 1
}

[ "$v_val" = "unlimited" ] && v_val=-1

echo "$v_val"
	
