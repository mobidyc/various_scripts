#!/bin/bash

# check if an iptables kernel module is loaded
# echo "loaded" or "unloaded" at the end

for m in $(find /lib/modules/$(uname -r)/kernel/net/ipv[46]/netfilter -name '*.ko')
do
	mod=$(basename "${m%%.ko}")

	[ "$(lsmod |grep -cw "$mod")" -gt "0" ] && {
		echo "loaded"
		exit
	}
done

echo "unloaded"
