#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

import os
import sys
import subprocess
import multiprocessing
import pprint
import re
import time
import getopt
import warnings

try:
    import json
except ImportError:
    import simplejson as json

import platform

from functions import parse_sshd_conf
from functions import is_installed
from log import log as logging
import color
from resources.Config import Config
from common import read_file
from common import extract_script

__author__ = "Cedrick GAILLARD"
__copyright__ = "Copyright 2015, Scality"
__maintainer__ = "Cedrick GAILLARD"
__email__ = "cedrick.gaillard@scality.com"
__status__ = "dev"

warnings.filterwarnings("ignore")


class Check(object):
    """ A check has to be populated with:
    - a category (hardware, system, network, whatever...)
    - a description of the check (ie. "kernel version")
    - the current value or a method to retrieve the current value

    the following functions will set the status of the check ("important" by default)
    - one or more expected value prefixed by one of these prefix and a
    semi-colon (ie. lesser_or_eq:2, , equal:"foo", fail_if_not_in_range:5:10)
      - self.equal(value)                       # alpha or numeric
      - self.notequal(value)                    # alpha or numeric
      - self.lesser_or_eq(value)                # inclusive, numeric only
      - self.greater_or_eq(value)               # inclusive, numeric only
      - self.fail_if_not_in_range(value_min, value_max) # inclusive, num only
    crit can be:
      - important
      - recommended

    these functions can be run multiple times to check for example a minimum
    value and a recommended value, in case of an important status has already
    been found, next steps will not be processed.
    """

    """ TODO:
        - convert values to appropriate type (int, long, float)
        - permit usage of complex values
    """

    def __init__(self, ID, *args, **kwargs):
        """ Object initialization """
        self.ID = ID
        self.category = Config.default_category
        self.subcategory = Config.default_subcategory
        self.current_value = None
        self.status = "unknown"
        self.msg = ["         ID: ──> " + str(ID) + " <──"]
        self.command_to_run = None
        self.command_type = "shell"  # ___Default environment, can be "python" also
        self.tests = []
        self.test_stored = False  # ______If True, tests are executed. At default, tests are appended to self.tests[]
        self.cmd_displayed = False
        self.duplicate_cmd = None
        self.log = logging()
        self.can_be_run = True  # ________If the command_to_run can not be executed because of a condition not met.
        self.condition_id = None  # ______Can be the ID of another check, set by Checks() daddy
        self.descr = ""  # _______________Description of this check
        self.done = False
        self.check_obj = {"id": self.ID}
        self.fix = None  # _______________a script to fix or a text indicating how to fix
        self.fixtype = "text"  # _________"text" or "script"
        self.fixisauto = False

        for key in ('current_value', 'category', 'subcategory'):
            if key in kwargs:
                setattr(self, key, kwargs[key])

    def set_fix(self, todo, **kwargs):
        """
        Fix:
        if type=text, isauto=false
        if type=script and isauto=false (default), a confirmation will be asked before fixing
        if type=script and isauto=true, the fix will be applied without interaction
        """
        self.fix = todo

        if "type" in kwargs:
            self.fixtype = kwargs["type"]

        # If fix is not a script we cannot be auto anyway
        if self.fixtype is "script" and "isauto" in kwargs:
            self.fixisauto = kwargs["isauto"]

    def set_id(self, ID):
        self.ID = ID
        self.msg = ["         ID: ──> " + str(ID) + " <──"]
        self.check_obj = {"id": self.ID}

    def set_currentVal(self, current_value=None):
        """
        Set the current value, permits to bypass a system command
        """
        if current_value is not None:
            self.current_value = str(current_value).strip()
        else:
            self.current_value = current_value
        return True

    def set_duplicate_cmd(self, cmd):
        """
        Set the cmd used to duplicate a Check(), run from the Checks() daddy
        """
        self.duplicate_cmd = cmd

    def set_condition_id(self, cmd):
        """
        Set the cmd used to condition this Check(), run from the Checks() daddy
        """
        self.condition_id = cmd

    def set_description(self, descr):
        """
        Set the description
        """
        self.descr = descr

    def set_category(self, category):
        """
        assign a category
        """
        self.category = category

        return True

    def set_subcategory(self, subcategory):
        """
        assign a category
        """
        self.subcategory = subcategory
        return True

    def to_run(self, command, **kwargs):
        """
        the "command" to run to populate the "current_value".
        At default, the "command" is identified as a shell command.
        it can be overriden with the argument:
        "type=python"
        to use an internal python method or function (ie. not a script)
        """
        if kwargs.get("type") == "python":
            self.command_type = "python"

        if self.command_type == "python":
            self.command_to_run = command
        else:
            self.command_to_run = extract_script(command)
        return True

    def run_the_command(self):
        """ Run the system/python command to populate current_value """
        self.log.std("\"%s\" Getting current value" % self.ID, remove_last=True)

        if not self.can_be_run:
            return True

        if self.current_value is not None:
            return True

        if self.command_to_run is None:
            self.message("Please add the needed command to retrieve the current value")
            return False

        if self.command_type == "shell":
            try:
                p = subprocess.Popen(self.command_to_run, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                self.current_value = p.stdout.read()
                retval = p.wait()
                del retval
            except:
                print "ERROR running the shell command: {0}".format(self.command_to_run)
                self.status = "important"
                self.can_be_run = False
                return False

        elif self.command_type == "python":
            try:
                self.current_value = eval(self.command_to_run)
            except:
                print "ERROR running the python command: {0}".format(self.command_to_run)
                self.status = "important"
                self.can_be_run = False
                return False

        self.current_value = str(self.current_value).strip()
        return True

    def message(self, expected, type=""):
        """ Format the text output messages """
        crit = color.erro("  IMPORTANT")
        unkn = color.erro("    UNKNOWN")
        warn = color.warn("RECOMMENDED")
        good = color.succ("       PASS")
        notr = color.cmd(" CANNOT RUN")
        p_id = ": ──> " + str(self.ID) + " <──"
        pval = "          system value: [" + color.warn(self.current_value) + "]" + type + "test value: " + color.succ(expected)
        comm = "    COMMAND:     " + color.cmd(self.command_to_run)

        if Config.verbosity == 0:
            nl = ""
        else:
            nl = "\n"

        """ Format a message according to its status """

        if not self.can_be_run:
            self.msg[0] = notr + p_id
            self.msg.append(nl + "      Because: ──> " + str(self.condition_id) + " <── does not pass")

        elif self.status == "important":
            self.msg[0] = nl + crit + p_id

            if Config.verbosity > 0:
                if not self.cmd_displayed and self.command_to_run is not None:
                    self.msg.append(comm)
                    self.cmd_displayed = True
                self.msg.append(crit + pval)

        elif self.status == "recommended":
            self.msg[0] = nl + warn + p_id

            if Config.verbosity > 0:
                if not self.cmd_displayed and self.command_to_run is not None:
                    self.msg.append(comm)
                    self.cmd_displayed = True
                self.msg.append(warn + pval)

        elif self.status == "good":
            self.msg[0] = nl + good + p_id

            if Config.verbosity > 1:
                if not self.cmd_displayed and self.command_to_run is not None:
                    self.msg.append(comm)
                    self.cmd_displayed = True
                self.msg.append(good + pval)

        elif self.status == "unknown":
            self.msg[0] = nl + unkn + p_id

        else:
            self.msg[0] = nl + unkn + p_id
            self.msg.append("               status is: " + color.warn(self.status))

    def equal(self, expected, status="important"):
        """ True if self.current_value is the same the expected value """

        # store for later use
        if not self.test_stored:
            return self.tests.append(["equal", expected, status])

        if not self.fc(status):
            return False

        # Need to check if it is an int or str
        try:
            xpect = float(expected)
            curr = float(self.current_value)
        except ValueError:
            xpect = expected
            curr = self.current_value

        if curr == xpect:
            self.status = "good"
            self.message(expected, " [ is equal as ] ")
        else:
            self.status = status
            self.message(expected, " [ is not equal as ] ")

        return True

    def regex(self, expected, status="important"):
        # store for later use
        if not self.test_stored:
            return self.tests.append(["regex", expected, status])

        if not self.fc(status):
            return False

        if re.match(expected, str(self.current_value)):
            self.status = "good"
        else:
            self.status = status

        self.message(expected, " [regex] ")
        return True

    def notequal(self, expected, status="important"):
        """ True if self.current_value is different the expected value """

        # store for later use
        if not self.test_stored:
            return self.tests.append(["notequal", expected, status])

        if not self.fc(status):
            return False

        # Need to check if it is an int or str
        try:
            xpect = float(expected)
            curr = float(self.current_value)
        except ValueError:
            xpect = expected
            curr = self.current_value

        if curr != xpect:
            self.status = "good"
            self.message(expected, " [ is different than ] ")
        else:
            self.status = status
            self.message(expected, " [ is not unequal as ] ")

        return True

    def lesser_or_eq(self, expected, status="important"):
        """ True if self.current_value is lesser or equal the expected value """

        # store for later use
        if not self.test_stored:
            return self.tests.append(["lesser_or_eq", expected, status])

        if not self.fc(status):
            return False

        try:
            xpect = float(expected)
            curr = float(self.current_value)
        except ValueError:
            self.status = "important"
            self.message("Numeric values expected!")
            return False

        if curr <= xpect:
            self.status = "good"
            self.message(expected, " [ is <= than ] ")
        else:
            self.status = status
            self.message(expected, " [ is not <= than ] ")

        return True

    def greater_or_eq(self, expected, status="important"):
        """ True if self.current_value is greater or equal the expected value """

        # store for later use
        if not self.test_stored:
            return self.tests.append(["greater_or_eq", expected, status])

        if not self.fc(status):
            return False

        try:
            xpect = float(expected)
            curr = float(self.current_value)
        except ValueError:
            self.status = "important"
            self.message("Numeric values expected!")
            self.status = "important"
            return False

        if curr >= xpect:
            self.status = "good"
            self.message(expected, " [ is >= than ] ")
        else:
            self.status = status
            self.message(expected, " [  is not >= than ] ")

        return True

    def fail_if_not_in_range(self, minimum, maximum, status="important"):
        # store for later use
        if not self.test_stored:
            return self.tests.append(["fail_if_not_in_range", minimum, maximum, status])

        if not self.fc(status):
            return False

        try:
            mini = float(minimum)
            maxi = float(maximum)
            curr = float(self.current_value)
        except ValueError:
            self.status = "important"
            self.message("Numeric values expected!")
            return False

        if mini <= curr <= maxi:
            self.status = "good"
        else:
            self.status = status

        self.message(str(minimum) + " - " + str(maximum), " [fail if not in range (inclusive)] ")
        return True

    def fc(self, status):
        """ Commons checks used by the test functions """

        if not self.can_be_run:
            return False

        if self.current_value is None:
            self.message("Please, populate the current value to check against")
            return False

        # if we already are important on a test, do not continue other tests
        if self.status == "important":
            return False

        if status != "recommended" and status != "important" and status != "unknown":
            self.status = "unknown"
            self.message("Please, define a correct criticity ('important' or 'recommended') for this test : " + status + " received")
            self.status = "important"
            return False

        return True

    def construct_json(self):
        self.check_obj.update({
            "category": self.category,
            "subcategory": self.subcategory,
            "status": self.status,
            "canrun": self.can_be_run,
            "description": self.descr,
            "condition": self.condition_id,
            "fix": self.fix,
            "fixtype": self.fixtype,
            "fixisauto": self.fixisauto,
            "command": {
                "cmd": self.command_to_run,
                "type": self.command_type,
                "val": self.current_value
            },
            "tests": []
        })

        for t in range(len(self.tests)):
            tt = self.tests[t]
            if "fail_if_not_in_range" == tt[0]:
                self.check_obj["tests"].append({"order": t, "name": tt[0], "val": tt[1], "val2": tt[2], "importance": tt[3]})
            else:
                self.check_obj["tests"].append({"order": t, "name": tt[0], "val": tt[1], "importance": tt[2]})

    def so_what(self):
        """ compare the current value with expected values """

        if self.done:
            return True

        if self.duplicate_cmd and self.duplicate_cmd != "True":
            return True

        if self.current_value is None:
            if self.command_to_run is None:
                self.message("Please add the needed command to retrieve the current value")
                return False
            else:
                if not self.run_the_command():
                    return False

        if self.current_value is None and self.can_be_run:
            self.message("Sorry, i can't figure out how to retrieve the current value :/")

        # We run the tests only if we have a current value for the current test
        if self.current_value is not None:
            self.test_stored = True
            for t in range(len(self.tests)):
                tt = self.tests[t]
                if "equal" == tt[0]:
                    self.equal(tt[1], tt[2])
                elif "regex" == tt[0]:
                    self.regex(tt[1], tt[2])
                elif "notequal" == tt[0]:
                    self.notequal(tt[1], tt[2])
                elif "lesser_or_eq" == tt[0]:
                    self.lesser_or_eq(tt[1], tt[2])
                elif "greater_or_eq" == tt[0]:
                    self.greater_or_eq(tt[1], tt[2])
                elif "fail_if_not_in_range" == tt[0]:
                    self.fail_if_not_in_range(tt[1], tt[2], tt[3])
            self.test_stored = False

        if Config.only_failed and self.status == "good":
            return True

        # Need to init the structure in case of 'can_be_run = False'
        if not self.can_be_run:
            self.message("")

        self.log.std("\"%s\" is done" % (self.ID), remove_last=True)
        # time.sleep(0.4)

        for line in self.msg:
            self.log.file("%s\n" % (line), remove_last=True)

        self.done = True

        self.construct_json()

        return True
