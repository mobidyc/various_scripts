#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=3:shiftwidth=3:smarttab:expandtab:softtabstop=3:autoindent

__author__       = "Cedrick GAILLARD"
__copyright__    = "Copyright 2015, Scality"
__maintainer__  = "Cedrick GAILLARD"
__email__        = "cedrick.gaillard@scality.com"
__status__       = "dev"

import os
import re
import sys
import platform

try:
    import yum
except:
    pass

try:
    from apt import Cache
    # from apt import package
except:
    pass


"""
"" Below are generic functions we can use in our checks to get the current value on the system
"""


def parse_sshd_conf(filename, key):
    """ parse an sshd_config style file
    search for the key and return the last associated value found
    """
    try:
        f = open(filename, 'r')
    except IOError, e:
        print e
    else:
        lines = f.readlines()
        value = ""

        for line in lines:
            # no extra whitespaces
            line = line.strip()

            # no comment
            if not re.search( "^#", line):
                # no empty lines
                if not re.search( "^$", line):
                    m = re.search("^(\S+?)[\s=]+(.*?)\s*$", line)
                    if m:
                        if m.group(1) == key:
                            value = m.group(2)

        f.close()
        return value


def is_installed(name):
    """ Search if one the name (pipe separated) is installed
    usually search for a Centos and Ubuntu package as the
    pkg_names sometimes differ.
    Automatically search for the current distribution.
    """

    names = name.split("|")

    def apt_origin(names):
        ret = False

        try:
            c = Cache(memonly=True)
            for name in names:
                try:
                    if c[name].is_installed:
                        ret = True
                except:
                    pass
                if ret:
                    break
        except:
            pass
        finally:
            c.close()

        return ret

    def yum_origin(names):
        yb = yum.YumBase()

        orig_stdout = sys.stdout
        sys.stdout = open(os.devnull, "w")
        for name in names:
            i = yb.rpmdb.searchNevra(name=name)
            if i:
                break
        sys.stdout = orig_stdout

        if i:
            return True
        return False

    distrib = platform.linux_distribution()[0]
    if distrib == "CentOS" or distrib == "CentOS Linux" or distrib == "Red Hat Enterprise Linux Server":
        if yum_origin(names):
            return True
    elif distrib == "Ubuntu":
        if apt_origin(names):
            return True

    return False
