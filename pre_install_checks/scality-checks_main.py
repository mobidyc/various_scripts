#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

"""scality-checks_main.py: Will check hardware and software before installing a Scality product"""

__author__       = "Cedrick GAILLARD"
__copyright__    = "Copyright 2015, Scality"
__maintainer__   = "Cedrick GAILLARD"
__email__        = "cedrick.gaillard@scality.com"

import os
import errno
import re
import sys
import subprocess
import shutil
import signal
import zipfile

os.environ["PATH"] += os.pathsep + "/sbin"
os.environ["PATH"] += os.pathsep + "/usr/sbin"
os.environ["PATH"] += os.pathsep + "/usr/local/sbin"

try:
    from resources.app import getpass
    from resources.app import pexpect
except:
    pass

try:
    import json
except ImportError:
    import simplejson as json

from resources.Config import Config
from resources.Checks import Checks
from resources.log import log as logging
from resources.common import local_infos
from resources.common import importJsonFile
from resources.common import byteify
from resources.obj_export import obj_export
from resources.parse_args import parse_args, set_remote_arg


def import_tests(log):
    """
    This function try to import all generic tests,
    then try to import some additional tests. Exit if dry run is activated
    """
    # Import the Generic tests.
    log.std("Importing the generic checks\n", remove_last=True)

    tests = Checks(obj_file=Config.included_tests)
    # tests = Checks(testdef_file=Config.included_defs)

    # Possibly import other tests.
    for i in range(len(Config.OBJFILES)):
        log.std("Importing additional checks from: %s\n" % (Config.OBJFILES[i]), remove_last=True)
        tests.importObjFile(Config.OBJFILES[i])
    for i in range(len(Config.JSFILES)):
        log.std("Importing additional checks from: %s\n" % (Config.JSFILES[i]), remove_last=True)
        tests.importJsonFile(Config.JSFILES[i])

    log.std("All additional checks imported successfully\n", remove_last=True)

    # Dry run
    if not Config.allow_running:
        if Config.display_json_tests:
            json_tests = json.dumps(tests.output_checks(), sort_keys=True, indent=3)
            log.std(json_tests + "\n")
        else:
            tests.getChecks(*tuple(Config.norun_args), cat=Config.cat, sub=Config.sub,
                            notcat=Config.notcat, notsub=Config.notsub)
        sys.exit(0)
    return tests


def Main(*argv):
    # Remove the forced cat & sub
    Config.notcat = Config.category_to_run_alone
    Config.notsub = Config.subcateg_to_run_alone

    # parse the arguments and define the Config variables
    if parse_args(*argv) is False:
        usage()
        sys.exit()

    # Initialize the logging class
    log = logging()
    log.init()

    # If sudo is asked, re run myself with sudo activated
    if os.geteuid() > 0 and len(Config.sudo) > 0:
        sys.argv.insert(0, 'sudo')
        rval = subprocess.call(sys.argv)
        sys.exit(rval)

    loaded_tests = import_tests(log)

    # Empty the logs
    empty_logs()

    Config.obj = {"nodes": []}

    # Test remote access by deploying myself if needed
    if Config.servers_json and not Config.obj_only:
        ips = []
        for customer in Config.servers_json:
            for servertype in Config.servers_json[customer]:
                test_access = remote_access(Config.servers_json[customer][servertype])
                ips.append(Config.servers_json[customer][servertype]['ip'])
                if test_access is False:
                    log.std("cannot access to a server\n")
                    sys.exit(1)
                else:
                    Config.servers_json[customer][servertype].update({"pwd": test_access})
        Config.ipaddresses.join(ips)

    # Run remotely or locally
    if Config.servers_json:
        for customer in Config.servers_json:
            customer_dirlog = os.path.join(Config.dirlog, customer)
            Config.customer = customer

            try:
                os.makedirs(customer_dirlog)
            except OSError as exc:
                if exc.errno == errno.EEXIST and os.path.isdir(customer_dirlog):
                    pass
                else:
                    raise

            all_obj_output = os.path.join(customer_dirlog, "global.json")
            try:
                shutil.copy("/dev/null", all_obj_output)
            except (IOError, shutil.Error) as e:
                log.std(e)
                sys.exit(1)

            for servertype in Config.servers_json[customer]:
                server_conf = Config.servers_json[customer][servertype]
                ip_list = server_conf["ip"].split(",")
                for ipaddress in ip_list:
                    ipaddress = ipaddress.strip()
                    stock_obj_file = os.path.join(customer_dirlog, servertype + "-" + ipaddress + ".obj")
                    Config.role = servertype

                    if is_local(ipaddress):
                        # Run locally
                        temp_obj_output = Config.obj_output
                        if not Config.obj_only:
                            log.std("Run the tests locally (%s)\n" % ipaddress, remove_last=True)
                            if "resource" in server_conf:
                                if server_conf.get("resource_type") == "obj":
                                    set_remote_arg(" --objfile ", server_conf["resource"])
                                else:
                                    set_remote_arg(" --jsfile ", server_conf["resource"])
                            if os.path.isfile(os.path.join(Config.tests_folder, Config.role + ".obj")):
                                loaded_tests.importObjFile(os.path.join(Config.tests_folder, Config.role + ".obj"))
                            run_locally(loaded_tests)

                        if not my_moves(temp_obj_output, stock_obj_file):
                            sys.exit(53)
                    else:
                        if not Config.obj_only:
                            # Run remotely
                            log.std("Running program from {0}:{1} ... ".format(ipaddress, Config.remote_root_folder), remove_last=True)

                            login = server_conf.get("login", "root")
                            port = server_conf.get("port", 22)

                            additional_tests = ""
                            if "resource" in server_conf:
                                additional_tests = " -O " + server_conf["resource"]
                            ressource_file = os.path.join(Config.tests_folder, Config.role + ".obj")
                            if os.path.isfile(ressource_file):
                                additional_tests += " -O " + ressource_file

                            ssh_cmd = "ssh -tt -p {port} {login}@{ip}".format(port=port, login=login, ip=ipaddress)
                            exec_cmd = "echo {success}; cd \"{remote_folder}\"".format(success=Config.ssh_success, remote_folder=Config.remote_root_folder)
                            python_cmd = "{sudo} python {progname} {remote_args} {tests}".format(sudo=Config.sudo, progname=sys.argv[0], remote_args=Config.args_for_remote,
                                                                                                 tests=additional_tests)
                            cmd = "{0} \"{1}; {2}\"".format(ssh_cmd, exec_cmd, python_cmd)
                            if not ssh_host(ipaddress, cmd):
                                continue

                            log.std("Done, retrieving the log files\n")
                            remote_file = os.path.join(Config.remote_root_folder, Config.remote_dirlog, Config.obj_log)
                            cmd = "scp -r -P {0} {1}@{2}:\"{3}\" \"{4}\"" .format(port, login, ipaddress, remote_file, stock_obj_file)

                            scp_host(ipaddress, cmd)

                    append_obj(stock_obj_file, all_obj_output)
            obj_export(all_obj_output)
            if not my_moves(Config.html_output, os.path.join(customer_dirlog, Config.html_log)):
                sys.exit(55)

        log.std("\nOutput files can be found on: {0}/\n\n".format(customer_dirlog), remove_last=True)
    else:
        if not Config.obj_only:
            run_locally(loaded_tests)
        obj_export(Config.obj_output)

        str = "Output files can be found on: %s/\n" % Config.dirlog
        log.std(str, remove_last=True)
        if Config.quick_display:
            try:
                f = open(Config.file_output, 'r')
            except IOError, e:
                log.std(e)
                sys.exit(1)

            for line in f:
                log.std(line)
            f.close()
        else:
            log.std("\nFor a quick view please run: # cat %s\n" % (Config.file_output))
            if not Config.nocolor:
                log.std("\nFor a nocolor view run: # sed 's@\\x1B\\[[0-9;]*[mK]@@g' %s\n" % (Config.file_output))

    archive = compress_folder(Config.remote_dirlog)
    log.std("")
    log.std("Please send the {archive} file to your Scality contact".format(predir=Config.root_folder, archive=archive))


def append_obj(src, dst):
    log = logging()
    """ Takes two json files join them and write the result to dst """

    if not os.path.isfile(src):
        return False

    # if dst exists, read it
    tempall = {"nodes": []}
    if os.path.isfile(dst) and os.stat(dst).st_size != 0:
        log.debug("os.path.isfile(dst): {0}".format(dst))
        tempall = importJsonFile(dst)

    tempone = importJsonFile(src)
    tempone.get("nodes")[0].update({"role": Config.role, "customer": Config.customer})
    tempall["nodes"].append(tempone["nodes"][0])

    try:
        fd = open(dst, 'w')
        if Config.DEBUG > 0:
            json.dump(tempall, fd, sort_keys=True, indent=4, ensure_ascii=False)
        else:
            json.dump(byteify(tempall), fd, ensure_ascii=False)
        fd.flush()
    except IOError, e:
        log.std(e)
        sys.exit(1)
    else:
        fd.close()


def run_locally(checks_obj):
    log = logging()
    log.open_logs()

    checks_obj.create_dups()
    checks_obj.run_all(cat=Config.category_to_run_alone, sub=Config.subcateg_to_run_alone)
    checks_obj.run_all(cat=Config.cat, sub=Config.sub, notcat=Config.notcat, notsub=Config.notsub)
    checks_obj.display_counter()

    obj_to_json(checks_obj)


def obj_to_json(checks_obj):
    log = logging()
    info = local_infos()
    node = {"hostname": info.hostname(), "role": Config.role}

    node.update(checks_obj.myobj)

    node.update({"nic": []})
    nics = info.ifaces()
    for nic in range(len(nics)):
        if nics[nic][1] != "127.0.0.1":
            node["nic"].append({"name": nics[nic][0], "ip": nics[nic][1]})

    node.update({"disk": []})
    drives_list = info.drive_list()
    for drive_list in iter(drives_list):
        drives = dict(item.split("=") for item in drive_list.split())
        if drives['TYPE'] == '"disk"':
            node["disk"].append({"name": drives['KNAME'].strip('"'), "drives": drives['SIZE'].strip('"'), "rota": drives['ROTA'].strip('"')})

    node.update({"memory": {"size": info.mem_size()}})
    node.update({"os": {"uname": info.uname()}})
    node.update({"cpu": {"arch": info.cpu()['arch'][0], "count": info.cpu()['count']}})

    Config.obj["nodes"].append(node)

    log.jsonlog(Config.obj)


def usage():
    """ Pretty print the usage of this program on stdout """
    log = logging()
    message = """
Usage: {progname} [options]

    Recommended Options:
    \t-f, --file <serverlist.json> __ A file containing server(s) list on which we will connect to run tests remotely.
    \t-R, --sudo: ___________________ encapsulat the program through sudo (nopasswd only, usually for remote use).

    Basic Options:
    \t-h, --help: ___________________ Displays the usage.
    \t-n, --no-run: _________________ Do not run the tests. Usually used to validate the check file configurations.
    \t-v, --verbose: ________________ Verbose output. can be dubbed for more output.
    \t-w, --with-color: _____________ Activate the colors in text log file.
    \t-c, --display-command: ________ Display the system commands used by the imported checks. Implies --no-run.
    \t-t, --display-test: ___________ Display the imported tests. Implies --no-run.
    \t-q, --quiet: __________________ Will not display succeeded tests.
    \t-Q, --quick: __________________ Will display the local checks on terminal.

    Advanced Options:
    \t-C, --category=<cat> __________ Restrict tests to this category, use it several times for cumulation.
    \t-S, --sub-category=<cat> ______ Restrict tests to this sub-category, use it several times for cumulation.
    \t-d, --discard-category=<cat> __ Restrict tests to not this category, use it several times for cumulation.
    \t-D, --discard-subcategory=<cat> Restrict tests to this not sub-category, use it several times for cumulation.
    \t-O, --objfile=<filename>: _____ A file containing tests in object format. Please refer to the Generic test file for examples: {generic_file}
    \t-J, --jsfile=<filename>: ______ A file containing tests in JSON format. Please, use the --json option to get an example (Deprecated, Remove planned).
    \t-j, --json: ___________________ Output all the checks in json format. Implies --no-run (Deprecated, Remove planned).
    \t-o, --output=<dir>: ___________ Change the output dir for the results, please prefer to use an absolute path when you use the remote functionnalities.
    \t-T, --trace: __________________ DEBUG mode.
    \t-p, --process-logs: ___________ based on the "-f" flag, recreate the global.html file based ont the logs.

    Additional informations:
    \t- Any additional tests (imported from files) will supersede the existing test.
    \t- The CHECK_CONFIG.py file should be filled with the appropriate customer environment when using the '-f' flag:

    Usage examples:
    \tRun all tests but the 'Perfs' subcategory:
    \t\t{progname} -D Perfs

    \tRun the 'Perfs' and 'Security' subcategories:
    \t\t{progname} -S Perfs -S Security

    \tRun all tests but the 'Perfs' subcategory, on a list of servers:
    \t\t{progname} -D Perfs -f remote.json
""".format(progname=sys.argv[0], generic_file=Config.generic_test_file)
    log.std(message, remove_last=True)


def scp_host(id, cmd):
    log = logging()
    log.debug(cmd + "\n")
    p = pexpect.spawn(cmd, timeout=300)
    # p.maxsize = 1
    i = p.expect([
        Config.ssh_newkey_question,
        'password:',
        "100%",
        pexpect.EOF
    ])

    if i == 0:
        p.sendline('yes')
        i = p.expect([
            Config.ssh_newkey_question,
            'password:',
            "100%",
            pexpect.EOF
        ])

    cnx_rval = True
    max = 5
    passwd = None
    while cnx_rval:
        if i == 1:
            if id in Config.passwds and Config.passwds[id] is not None:
                passwd = Config.passwds[id]
            else:
                # For the first passwd asking, Try the last remembered pwd
                if max == 5 and Config.last_pwd is not None:
                    passwd = Config.last_pwd
                else:
                    passwd = getpass.getpass()
            p.sendline(passwd)
            i = p.expect([
                Config.ssh_newkey_question,
                'password:',
                "100%",
                pexpect.EOF
            ])
            continue
        elif i == 2:
            break
        elif i == 3:
            log.std("Connection error, command was\n")
            log.std(cmd + "\n")
            log.std(p.before + "\n")
            cnx_rval = False

        if --max <= 0:
            break

    p.expect(pexpect.EOF)

    Config.passwds.update({id: passwd})
    Config.last_pwd = passwd

    p.close()

    return cnx_rval


def ssh_host(id, cmd):
    log = logging()
    log.debug(cmd + "\n")
    p = pexpect.spawn(cmd, timeout=300)
    # p.maxsize = 1
    i = p.expect([
        Config.ssh_newkey_question,
        'password:',
        Config.ssh_success,
        pexpect.EOF
    ])

    if i == 0:
        p.sendline('yes')
        i = p.expect([
            Config.ssh_newkey_question,
            'password:',
            Config.ssh_success,
            pexpect.EOF
        ])

    cnx_rval = True
    max = 5
    passwd = None
    while cnx_rval:
        if i == 1:
            if id in Config.passwds and Config.passwds[id] is not None:
                passwd = Config.passwds[id]
            else:
                # For the first passwd asking, Try the last remembered pwd
                if max == 5 and Config.last_pwd is not None:
                    passwd = Config.last_pwd
                else:
                    passwd = getpass.getpass()
            p.sendline(passwd)
            i = p.expect([
                Config.ssh_newkey_question,
                'password:',
                Config.ssh_success,
                pexpect.EOF
            ])
            continue
        elif i == 2:
            break
        elif i == 3:
            log.std(p.before + "\n")
            log.std("I either got key or connection timeout, command was:\n")
            log.std(cmd + "\n")
            p.close()
            cnx_rval = False

        if --max <= 0:
            break

    log.std("...")

    # p.logfile = sys.stdout
    p.expect(pexpect.EOF)
    p.close()

    Config.passwds.update({id: passwd})
    Config.last_pwd = passwd

    if not p.exitstatus == 0:
        log.std("Something goes wrong, rval: " + str(p.exitstatus) + "\n")
        log.std("command was: %s" % cmd)
        log.std(p.before + "\n")
        cnx_rval = False

    return cnx_rval


def remote_access(mydict):
    log = logging()
    if "ip" not in mydict:
        log.std("Missing IP address\n")
        sys.exit(1)

    if zipfile.is_zipfile(sys.argv[0]):
        transfer_file = sys.argv[0] + ".tar.gz"
        tar_cmd = [
            "tar",
            "czvf",
            transfer_file,
            sys.argv[0]
        ]

    else:
        transfer_file = Config.root_folder + ".tar.gz"
        tar_cmd = [
            "tar",
            "czf",
            transfer_file,
            "--exclude=TODO",
            "--exclude=cookiecutter.json",
            "--exclude=README.md",
            "--exclude=*.pyc",
            "--exclude=*/*.pyc",
            "--exclude=*/*/*.pyc",
            "--exclude=tags",
            "--exclude=TAGS",
            "--exclude=scality-results",
            "--exclude=toggle_post.sh",
            "--exclude=templates",
            "--exclude=*/.*",
            "--exclude=*.json",
            "../" + Config.root_folder
        ]

    try:
        FNULL = open(os.devnull, 'w')
        subprocess.call(tar_cmd, shell=False, stdout=FNULL, stderr=subprocess.STDOUT)
    except OSError as e:
        log.std(e)
        sys.exit(1)
    finally:
        FNULL.close()

    ip = mydict["ip"].split(",")

    login = mydict.get("login", "root")
    port = mydict.get("port", 22)

    for ipaddress in ip:
        ipaddress = ipaddress.strip()
        if is_local(ipaddress):
            log.std("{0} is a local address\n".format(ipaddress))
            continue

        log.std("deploying program to {0}:{1}\n".format(ipaddress, Config.remote_root_folder))

        cmd = "scp -P %s %s %s@%s:'%s'" % (port, transfer_file, login, ipaddress, "/tmp/")
        if not scp_host(ipaddress, cmd):
            return False

        ssh_cmd = "ssh -tt -p {port} {login}@{ip}".format(port=port, login=login, ip=ipaddress)
        exec_cmd = "cd /tmp ; tar xzf {tarfile}; echo {ssh_echo}".format(tarfile=transfer_file, ssh_echo=Config.ssh_success)
        cmd = "{0} {1}".format(ssh_cmd, exec_cmd)

        if not ssh_host(ipaddress, cmd):
            return False
    os.remove(transfer_file)


def compress_folder(folder):
    log = logging()
    if not os.path.isdir(folder):
        return False
    destination = os.path.join(folder, folder + ".tar.gz")

    if os.path.isfile(destination):
        os.remove(destination)

    tar_cmd = ["tar", "czvf", destination, "--exclude=" + destination, folder]
    try:
        FNULL = open(os.devnull, 'w')
        subprocess.call(tar_cmd, shell=False, stdout=FNULL, stderr=subprocess.STDOUT)
    except OSError as e:
        log.std(e)
        sys.exit(1)
    finally:
        FNULL.close()
    return destination


def is_local(ip):
    log = logging()
    """ search if "ip" is in the /proc/net/tcp file
    return True or False
    """
    # List local ip addresses
    local_hexa = {}
    try:
        tcplist = open('/proc/net/tcp', 'r')
    except IOError, e:
        log.std(e)
        sys.exit(2)
    else:
        for line in tcplist:
            parts = line.split()
            hexaip = parts[1].split(':')
            try:
                # simply test if it is text or hexa
                h = int(hexaip[0], 16)
                del h
            except (TypeError, ValueError):
                pass
            else:
                # Only check for IPv4
                if len(str(hexaip[0])) == 8:
                    local_hexa.update({hexaip[0]: ""})

    for hexa in local_hexa:
        # print "test ip: %s - hexa: %s - convert: %s" % (str(ip), str(hexa), str(convert_hexa_to_ip(hexa)))
        if ip == convert_hexa_to_ip(hexa):
            return True

    return False


def convert_hexa_to_ip(my_hex):
    iph = re.findall('..', my_hex)
    ipd = str(int(iph[3], 16)) + "." + str(int(iph[2], 16)) + "." + str(int(iph[1], 16)) + "." + str(int(iph[0], 16))
    return ipd


def signal_handler(signal, frame):
    """ function to trap ctrl+c """
    log = logging()
    log.__exit__()
    sys.exit(0)


def empty_logs():
    log = logging()
    try:
        if os.path.isfile(Config.file_output):
            shutil.copy("/dev/null", Config.file_output)
        if not Config.obj_only:
            if os.path.isfile(Config.obj_output):
                shutil.copy("/dev/null", Config.obj_output)
            if os.path.isfile(Config.html_output):
                shutil.copy("/dev/null", Config.html_output)
    except IOError, e:
        log.std(e)
        sys.exit(1)
    except shutil.Error, e:
        log.std(e)
        sys.exit(1)


def my_moves(src, dst):
    log = logging()
    try:
        shutil.move(src, dst)
    except (IOError, shutil.Error) as e:
        log.std(e)
        return False

    log.debug("moved: " + src + " to " + dst + "\n")

    return True

if __name__ == '__main__':
    # trap ctrl+c
    signal.signal(signal.SIGINT, signal_handler)

    # run the main program
    Main(sys.argv[1:])
