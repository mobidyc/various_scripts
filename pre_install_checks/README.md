# README #


```
#!bash

Usage: ./scality-checks_main.py [options]

Recommended Options:
  -f, --file <serverlist.json> __ A file containing server(s) list on which we will connect to run tests remotely.
  -R, --sudo: ___________________ encapsulat the program through sudo (nopasswd only, usually for remote use).

Basic Options:
  -h, --help: ___________________ Displays the usage.
  -n, --no-run: _________________ Do not run the tests. Usually used to validate the check file configurations.
  -v, --verbose: ________________ Verbose output. can be dubbed for more output.
  -w, --with-color: _____________ Activate the colors in text log file.
  -c, --display-command: ________ Display the system commands used by the imported checks. Implies --no-run.
  -t, --display-test: ___________ Display the imported tests. Implies --no-run.
  -q, --quiet: __________________ Will not display succeeded tests.
  -Q, --quick: __________________ Will display the local checks on terminal.

Advanced Options:
  -C, --category=<cat> __________ Restrict tests to this category, use it several times for cumulation.
  -S, --sub-category=<cat> ______ Restrict tests to this sub-category, use it several times for cumulation.
  -d, --discard-category=<cat> __ Restrict tests to not this category, use it several times for cumulation.
  -D, --discard-subcategory=<cat> Restrict tests to this not sub-category, use it several times for cumulation.
  -O, --objfile=<filename>: _____ A file containing tests in object format. Please refer to the Generic test file for examples: Generics/included.py
  -J, --jsfile=<filename>: ______ A file containing tests in JSON format. Please, use the --json option to get an example (Deprecated, Remove planned).
  -j, --json: ___________________ Output all the checks in json format. Implies --no-run (Deprecated, Remove planned).
  -o, --output=<dir>: ___________ Change the output dir for the results, please prefer to use an absolute path when you use the remote functionnalities.
  -T, --trace: __________________ DEBUG mode.
  -p, --process-logs: ___________ based on the "-f" flag, recreate the global.html file based ont the logs.

Additional informations:
  - Any additional tests (imported from files) will supersede the existing test.
  - The CHECK_CONFIG.py file should be filled with the appropriate customer environment when using the '-f' flag:

Usage examples:
Run all tests but the 'Perfs' subcategory:
./scality-checks_main.py -D Perfs

Run the 'Perfs' and 'Security' subcategories:
./scality-checks_main.py -S Perfs -S Security

Run all tests but the 'Perfs' subcategory, on a list of servers:
./scality-checks_main.py -D Perfs -f remote.json

```

### What is this repository for? ###

* run tests on one or several server and display the status.
* status can be GOOD, RECOMMENDED, IMPORTANT, CANNOT_RUN or UNKNOWN.
* status outputs are logged in txt, json and html format.
* Pure python, no external dependencies.

### How do I get set up? ###

* Internal generic tests are implemented in Generics/included.py.
* a test can be superseded by copying a check definition in a file then importing this file with the --objfile argument (see "Generics" folder for examples).
* one or more servers with specific test file can be send to the program through a json configuration file (lab.json file for sample)

### TO DO LIST ###
- permits to use more than one set_condition_id
