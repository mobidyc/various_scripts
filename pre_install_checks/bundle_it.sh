#!/bin/bash

full_dir=$(dirname "$0")
[ "$full_dir" = "." ] && full_dir=$(pwd -P)

prog_dir=$(basename "$full_dir")

cd "$full_dir"
#git_commit=$(git rev-parse --short HEAD)
git_commit=$(git describe --tags --dirty --always)
dst_name="scality-${prog_dir}-${git_commit}.py"

test -f "${dst_name}" && rm "${dst_name}"
cd "$full_dir"

git log --pretty=format:"%h - %s" > CHANGELOG

zip -r "${dst_name}" . \
   -x '*.tar.gz' \
   -x TODO \
	-x '*gitHooks*' \
   -x cookiecutter.json \
   -x README.md \
   -x *temp* \
   -x '*temp/*' \
   -x 'scality-results/*' \
   -x '*.pyc' \
   -x "${prog_dir}-*.py" \
	-x '*tags*' \
	-x '*TAGS' \
	-x '*scality-results*' \
	-x toggle_post.sh \
	-x '*templates*' \
	-x '\.*' \
   -x *.json >/dev/null

zip -g "${dst_name}" template.json

echo
echo "Binary file created: '${dst_name}'"
