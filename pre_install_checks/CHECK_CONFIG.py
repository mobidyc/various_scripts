#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

"""
Please use this configuration file to customize the checks according your environment
"""

#
# default variables (we take the supervisor as default)
#
supervisor_ps = 2                  # number of power supply
supervisor_space_var = 167772160   # minimum available space in /var (in kbytes)
supervisor_free_memory = 33554432  # minimum amount of free memory
supervisor_cpu_number = 2          # number of CPU
supervisor_core_number = 2         # number of cores
supervisor_free_hdd = 0            # Number of not partitionned HDDs
supervisor_free_ssd = 0            # Number of not partitionned SSDs


# Connectors
connector_ps = 2                  # number of power supply
connector_space_var = 167772160   # minimum available space in /var (in kbytes)
connector_free_memory = 33554432  # minimum amount of free memory
connector_cpu_number = 2          # number of CPU
connector_core_number = 2         # number of cores
connector_free_hdd = 0            # Number of not partitionned HDDs
connector_free_ssd = 0            # Number of not partitionned SSDs


# Storage nodes
storage_ps = 2                  # number of power supply
storage_space_var = 209715200   # minimum available space in /var (in kbytes)
storage_free_memory = 33554432  # minimum amount of free memory
storage_cpu_number = 2          # number of CPU
storage_core_number = 2         # number of cores
storage_free_hdd = 4            # Number of not partitionned HDDs
storage_free_ssd = 2            # Number of not partitionned SSDs
