#!/bin/bash

Usage() {
  $0 [--limit <parallel_limit>] src dst
  exit
}

[ "${#}" -eq "0" ] && {
  Usage
}

case $1
in
  --limit) def_limit=$2
           shift 2
           ;;
esac

[ "${#}" -ne "2" ] && {
  Usage
}

src=$1
dst=$2

limit=${def_limit:-30}

echo "You will copy the content of $src/ in $dst/ with $limit threads."

while [ 1 ];
do
  /bin/echo "Are you ok with that [y/N]? \c"
  read ANSWER
  ANSWER=$(echo "$ANSWER" |tr "[A-Z]" "[a-z]")

  case $ANSWER
  in
    yes|y|oui|o) : ;;
    *) exit 1 ;;
  esac

mkdir -p /tmp/projects/pids

add_pid() {
  touch /tmp/projects/pids/$1
  ls -1 /tmp/projects/pids/
}
del_pid() {
  rm /tmp/projects/pids/$1
  ls -1 /tmp/projects/pids/
}

cd $src/
[ "$?" -ne "0" ] && exit1

ls -1 |while read project
do
  echo rsync --recursive --executability --sparse --whole-file --delete --delete-during --size-only --stats --human-readable --progress "$project" $dst/ > "/tmp/projects/${project}.log" &
  add_pid $! >/dev/null

  while [ "$(add_pid |wc -l)" -ge "$limit" ]
  do
    for p in $(add_pid)
    do
      ps -p $p &>/dev/null
      [ "$?" -ne "0" ] && {
        del_pid $p >/dev/null
      }
    done

    ps -ef |grep rsync |grep -v grep |grep -v PID
    echo
    sleep 2
  done
done

wait
rm -rf /tmp/projects/pids/*

