#!/bin/bash
#
# vim:tabstop=3:syntax=sh:
#
# ---------
## Name    : scality-add_hp_raid0.sh
## Subject : Using hpssacli, List all new disks, create raid0 for each, use scaldiskadd.
##----------------------------------------------------------------------------
# May 11 2015 = Creation : Cedrick GAILLARD
#-----------------------------------------------------------------------------

HPCLI="echo hpssacucli"
tmpdir=temp_add_hp_raid0

######HPCLI="echo /usr/sbin/hpssacli"
######tmpdir=$(mktemp -d)

show_conf="$tmpdir/ctrl_all_sh_conf"
show_conf_detail="$tmpdir/ctrl_all_sh_conf_detail"
list_new_disks="$tmpdir/list_new_disks"
list_all_disks="$tmpdir/all_disks"
to_be_formatted="$tmpdir/2beformat"
list_controllers="$tmpdir/ctrl_list"
pids="$tmpdir/pid_list"

# Ctrl list
######$HPCLI ctrl all show |sed 's@(.*@@' |grep -v "^$" |awk '{print $NF}' > $list_controllers

# Scan for new devices
$HPCLI rescan

# Search for new devices
echo "Temporary files will reside in $tmpdir"

cp /dev/null $list_new_disks
for slt in $(< $list_controllers)
do
	echo "Disable write cache if no battery for ctrl $slt"
	######$HPCLI ctrl slot=2 modify nobatterywritecache=disable
	######$HPCLI ctrl slot=$slt show config > ${show_conf}.slot_$slt
	awk '/unassigned/,/*/' ${show_conf}.slot_$slt |grep physicaldrive |sed "s@^@slot=${slt} @" >> $list_new_disks
done

list_type=$(awk -F, '{print $2, $3}' $list_new_disks |sort |uniq -c)

cat "${list_new_disks}"
echo "count:"
echo "$list_type"

echo -n "Is it OK to continue? [y|N] "
read so

# Check the disks or exit
so=$(echo $so |tr "[A-Z]" "[a-z]")
case $so
in
	"y"|"yes") echo ;;
	*) echo "This answer was not what i expected :/" && exit 0 ;;
esac

# Parallellize it
echo "Add the disks to Raid0 volume in progress..."
pid_list=
cp /dev/null $pids
while read slot type disk rest
do
	( $HPCLI ctrl $slot create type=ld drives=$disk raid=0 ) &
	p="$!"
	pid_list="$pid_list $p"
	echo $p >> $pids
done < $list_new_disks

# Wait until the volumes are created
while :
do
	pp=
	for p in $pid_list
	do
		isrunning=$(ps -ef |grep -w $p |grep hpssacli)
		[ -n "$isrunning" ] && {
			pp="$pp $p"
		}
	done

	[ -z "$pp" ] && break
	pid_list=$pp
	sleep 3
	echo -n "."
done

echo "Disks added, rescan"
$HPCLI rescan

for slt in $(< $list_controllers)
do
	echo "Enable caching for slot $slt"
	$HPCLI ctrl slot=$slt ld all modify arrayaccelerator=enable
done

echo "Identify new volumes"
######$HPCLI ctrl all show config detail > ${show_conf_detail}

grep -A17 'Disk Name' $show_conf_detail | \
	egrep 'Disk Name|physicaldrive' | \
	awk '{
		if ($1 ~ /Disk/) {
			print $3
		} else {
			print $2
		}
	}' |sed ':a;N;s/\n/ /g' > $list_all_disks

cp /dev/null $to_be_formatted
while read pdev phys
do
	######blockdev --rereadpt $pdev
	for new in $(awk '{print $3}' $list_new_disks)
	do
		[ "$phys" = "$new" ] && {
			# Double check, do not want to format an already sliced disk
			slice=$(ls -l /sys/block/${pdev}/*/dev 2>/dev/null)
			if [ -z "$slice" ]; then
				echo "$pdev $phys" >> $to_be_formatted
			else
				echo "Error detected, $pdev $phys contains slice(s), cannot work on it"
			fi
			break
		}
	done
done < $list_all_disks

echo
cat $to_be_formatted
echo "These disks will be formatted"
echo "If an SSD is found, it will be formatted first then spinned disks will use it as NVP"
echo -n "Do you agree? [y|N] "

read ok_for_format
ok_for_format=$(echo $ok_for_format |tr "[A-Z]" "[a-z]")
case $ok_for_format
in
	"y"|"yes") echo ;;
	*) echo "Abandon" && exit 0 ;;
esac

SSDS=$(grep -w "Solid" ${list_new_disks} |awk '{print $3}')
ssd_to_nvp=auto

if [ -n "$SSDS" ] ; then
	echo "SSD found"
	ssds=
	for phys in $SSDS
	do
		ssd=$(grep -w "$phys" $to_be_formatted |awk '{print $1}')
		[ -n "$ssd" ] && {
			######scaldiskadd --partition --fstab --mkfs $ssd
			echo scaldiskadd --partition --fstab --mkfs $ssd
		}
	done

	### Need to be improved
	ssd_to_nvp=$(echo $ssd |awk '{print $1}')
else
	echo "No SSD found"
fi


SPNS=$(grep -vw "Solid" ${list_new_disks} |awk '{print $3}')
[ -n "$SPNS" ] && {
	for phys in $SPNS
	do
		hdd=$(grep -w "$phys" $to_be_formatted |awk '{print $1}')
		[ -n "$hdd" ] && {
			######scaldiskadd --partition --fstab --mkfs --nvp $ssd_to_nvp $ssd
			echo scaldiskadd --partition --fstab --mkfs --nvp $ssd_to_nvp $hdd
		}
	done
}
