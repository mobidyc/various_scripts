#!/bin/bash

sysfs=/run/scality/connectors/sfused/misc
mode=${1:-chord}
fsops=~/fsops.sh
screenrc=/tmp/screenrc

proxy_count=1
proxy_url="http://sn%d:81/proxy/arc/.stats"

srest_ino=4
srest_cache=3
srest_drv=3

chord_ino=2
chord_cache=0
chord_drv=0

case $mode in
	srest)
		paths="stats_sparse:$srest_ino cache_ops:$srest_cache ring_drv_stats:$srest_drv"
		for i in $(seq $proxy_count)
		do
			url="$(printf $proxy_url $i)"
			paths="$paths $url"
		done
	;;
	chord)
		paths="stats_sparse:$chord_ino cache_ops:$chord_cache ring_drv_stats:$chord_drv"
	;;
	*)
		echo "invalid mode: $1 - expected 'chord' or 'srest'"
		exit 1
	;;
esac

screens_nr=0
echo "sessionname stats" > $screenrc

for p in $paths
do
	[ "$screens_nr" -gt "0" ] && {
		echo "split" >> $screenrc
		echo "focus down" >> $screenrc
	}

	case $p in
		http*)
			ev="dig get put del"
			echo "screen -t '$p sect 1' $screens_nr $fsops -f $p -w11 -s 1 -c '$ev' -t '$ev' -p '$ev'" >> $screenrc
			screens_nr=$(($screens_nr + 1))

			ev="ll_get_loc ll_put_loc"
			echo "screen -X stats -d -m -t '$p sect 2' $screens_nr $fsops -f $p -w11 -s 2 -c '$ev' -t '$ev' -p '$ev'">> $screenrc
		;;
		stats_sparse*)
			echo "screen -t $p $screens_nr $fsops -f $sysfs/$p -c 'create delete commit stat fsync read write' -t 'st_read st_write' -p 'cwrite st_read st_write'" >> $screenrc
		;;
		cache_ops*)
			echo "screen -t $p $screens_nr $fsops -f $sysfs/$p -c '' -p 'll_download ll_upload' -w11" >> $screenrc
		;;
		ring_drv_stats*)
			if [ $mode = srest ]; then
				ev="get put del"
				echo "screen -t $p $screens_nr $fsops -f $sysfs/$p -c '$ev' -t '$ev' -p '$ev'" >> $screenrc
			else
				ev="cv_get_df ll_get tcv_put_dft ll_put"
				echo "screen -t $p $screens_nr $fsops -f $sysfs/$p -c '$ev' -t '$ev' -p '$ev'" >> $screenrc
			fi
		;;
	esac

	screens_nr=$(($screens_nr + 1))
done

for i in $(seq 0 $screens_nr)
do
	[ "$screens_nr" -gt "0" ] && {
		echo "focus up" >> $screenrc
	}
done

echo "split -v" >> $screenrc
echo "focus right" >> $screenrc
echo "resize 40" >> $screenrc
echo "screen -t 'active conn' $screens_nr sh -c 'while sleep 1; do echo \$(date +%T ; cat -v $sysfs/conn |grep \"^active conn\" ); done'" >> $screenrc
screens_nr=$(($screens_nr + 1))

echo "focus down" >> $screenrc
echo "split -v" >> $screenrc
echo "focus right" >> $screenrc
echo "resize 35" >> $screenrc
echo "screen -t ifstat $screens_nr dstat -nt" >> $screenrc

echo "focus left" >> $screenrc

screen -c $screenrc

