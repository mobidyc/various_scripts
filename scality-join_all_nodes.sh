#! /bin/bash
#
# Script located in /usr/sbin/scality-join_all_nodes.sh
# version : 1.0
# Author  : Cedrick GAILLARD

# We want to check all the rings.
RINGS=$(ringsh supervisor ringList)
[ "$?" -ne "0" ] && echo "error: ringsh not responding" && exit 1

for ring in $RINGS
do
	# Do not continue if autojoin is off.
	AUTOJOIN=$(ringsh supervisor dsoConfigGet ${ring} join_auto |awk '{print $NF}')
	[ "$?" -ne "0" ] && echo "error: ${ring}: ringsh not responding" && continue
	[ "$AUTOJOIN" -eq "0" ] && continue

	GLOBAL_STATUS=$(ringsh supervisor ringStatus ${ring} |grep Nodes: |tr -d ,)
	RUNSTATUS=$(echo $GLOBAL_STATUS |egrep -o "RUN:[0-9]+" |cut -d: -f2)
	# If we have at least one RUNning nodem we do not need to continue
	[ -n "$RUNSTATUS" ] && {
		[ "$RUNSTATUS" -gt "0" ] && continue
	}

	OOSSTATUS=$(echo $GLOBAL_STATUS |egrep -o "OOS:[0-9]+" |cut -d: -f2)
	# Only if we have OOS (means ready to join) nodes.
	[ -n "$OOSSTATUS" ] && {
		# And of course we need at least one OOS node.
		[ "$OOSSTATUS" -eq "0" ] && continue
	}

	echo "Start to join the nodes on the ring ${ring}"
	ringsh supervisor nodeJoinAll $ring
done
