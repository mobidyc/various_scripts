#! /bin/bash
#
# Script located in /usr/sbin/scality-backup_confs_everywhere.sh
# version : 1.0
# Author  : Cedrick GAILLARD

####
# NOTE:
# This script has to run on the supervisor
# The supervisor should have passwordless ssh access to all servers including itself
# Simply add all servers you want to backup as argument (including the supervisor)
# All backups will be located in /srv/scality/backup-<host> with a 10 backups retention
###

# add hostnames/IPs to connect
hosts2backup=$*
# do we have host to backup
[ -z "$hosts2backup" ] && exit 1

###
# Default values
###
DEF_SRVDIR=/srv/scality                        # Where are store the pillars, installer, etc
DEF_BCKDIR=/srv/scality/scality_all_backups    # Where we store all backups
DEF_VARLIB=/var/lib/scality/backup/            # Where are stored the existing backups

timestamp=$(date +"%Y%m%d-%H%M")
SRVBACKUPDIR="${DEF_BCKDIR}/backup-$(uname -n)-srv"
SRVBACKUP=${SRVBACKUPDIR}/srv-${timestamp}.tar.gz

# backup /srv
mkdir -p ${SRVBACKUPDIR}
cd ${DEF_SRVDIR}
tar czf - --acls --atime-preserve --preserve-permissions --selinux --xattrs  installer pillar salt scripts  --exclude repository > ${SRVBACKUP} 2>/dev/null
if [ "$?" -ne "0" ] ; then
	echo "error backupping ${DEF_SRVDIR}"
fi

# try to backup all hosts in /srv and /srv in all hosts
for host in $hosts2backup
do
	test_cnx="$(ssh $host "uname -n")"
	if [ "$?" -ne "0" ] ; then
		echo "connection failed to $host"
		exit 1
	fi
	if [ -z "$test_cnx" ] ; then
		echo "there was a problem connecting to $host"
		exit 1
	fi

	CNFBACKUPDIR=${DEF_BCKDIR}/backup-$host-cnf
	CNFBACKUP=${CNFBACKUPDIR}/conf-${timestamp}.tar.gz

	mkdir -p $CNFBACKUPDIR
	cd ${CNFBACKUPDIR}
	if [ "$?" -ne "0" ] ; then
		echo "directory $CNFBACKUPDIR does not exists"
		continue
	fi
	ssh $host 'tar czf - --acls --atime-preserve --preserve-permissions --selinux --xattrs ${DEF_VARLIB} --exclude archives 2>/dev/null' > ${CNFBACKUP}

	# remove old conf backups
	nb_bcks=$(ls -1 |wc -l)
	if [ "$nb_bcks" -gt "10" ] ;then
		ls -1t |tail -n +11 |xargs -I{} rm -f {}
	fi

	if [ -f "${SRVBACKUP}" ] ; then
		ssh $host "mkdir -p ${SRVBACKUPDIR}"
		scp ${SRVBACKUP} ${host}:${SRVBACKUP} >/dev/null

		# remove old srv backups
		nb_bcks=$(ssh $host "cd ${SRVBACKUPDIR} && ls -1 |wc -l")
		if [ "$nb_bcks" -gt "10" ] ;then
			ssh $host "cd ${SRVBACKUPDIR} && ls -1t |tail -n +11 |xargs -I{} rm -f {}"
		fi
	fi
done

# keep only 10 local backups
cd ${SRVBACKUPDIR}
nb_bcks=$(ls -1 |wc -l)
if [ "$nb_bcks" -gt "10" ] ;then
   ls -1t |tail -n +11 |xargs -I{} rm -f {}
fi
