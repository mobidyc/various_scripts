#!/bin/bash

Usage()
{
	cat << EOT
	$0 <load>
	load can be:
		-default
		-medium
		-smooth
		-low
EOT
	exit ${1:-0}
}

chordctrlmaxparalleltasks=
chunkapimaxphysdelete=

while [ "$#" -ne "0" ]
do
	case $1
	in
		"default") chordctrlmaxparalleltasks=15 ; chunkapimaxphysdelete=64 ; shift 1 ;;
		"medium") chordctrlmaxparalleltasks=10 ; chunkapimaxphysdelete=32 ; shift 1 ;;
		"smooth") chordctrlmaxparalleltasks=5 ; chunkapimaxphysdelete=8 ; shift 1 ;;
		"low") chordctrlmaxparalleltasks=2 ; chunkapimaxphysdelete=5 ; shift 1 ;;
		"-h"|"--help") Usage ;;
		*) Usage 1 ;;
	esac
done

if [ -z "$chordctrlmaxparalleltasks" ] ; then
	Usage 1
fi

def_file=$(mktemp)
cat > "$def_file" << EOF
node configSet msgstore_protocol_chord chordctrlmaxparalleltasks $chordctrlmaxparalleltasks
node configSet msgstore_storage_chunkapi chunkapimaxphysdelete $chunkapimaxphysdelete
EOF

rings=$(ringsh supervisor ringList)
for ring in $rings
do
	for node in $(ringsh supervisor dsoStatus $ring |grep Node: |awk '{print $2}')
	do
		ringsh -r $ring -u $node -f "$def_file"
	done
done

rm $def_file


