#!/bin/bash

manufacturer=$(dmidecode -t 1 | grep -Po "Manufacturer: .*" |cut -d: -f2)
product_name=$(dmidecode -t 1 | grep -Po "Product Name: .*" |cut -d: -f2)

os_release=$(cat /etc/os-release | grep -Po '(?<=PRETTY_NAME=).*' |tr -d '"')

os_disk=$(df -P / |tail -1 |awk '{print $1}')
os_disk=$(readlink -f $os_disk |awk -F/ '{print $NF}')
os_disk_model=$(cat /sys/class/block/*/$os_disk/../device/model)
os_disk_vendor=$(cat /sys/class/block/*/$os_disk/../device/vendor)

which hpssacli &>/dev/null
if [ "$?" -eq "0" ]
then
	cd /tmp
	slots="$(hpssacli ctrl all show |grep -Po "(?<=Slot )\d+")"
	for slot in $slots; do
		hpssacli ctrl slot=$slot show config detail > /tmp/tmpfile
		csplit -f scality_raidinfo- -s /tmp/tmpfile '/Array:/' '{*}'

		for file in $(ls -1 scality_raidinfo-*)
		do
			if [[ "$file"  = "scality_raidinfo-00" ]]
			then
				card_name=$(grep -P "in Slot $slot" $file |sed "s/ in Slot $slot.*//")
				card_cache_size=$(grep -P "Total Cache Size:" $file |sed 's/.*Size: //' |awk '{print $1}')
				card_battery_count=$(grep -P "Battery/" $file |grep Count |awk '{print $NF}')
				card_battery_status=$(grep -P "Battery/" $file |grep Status |awk '{print $NF}')
			else
				disk_name=$(sed 's/^[ ]*//g' $file |awk '/Array/,/Enclosure/' |grep "Disk Name:" |cut -d' ' -f3)
				disk_intf=$(sed 's/^[ ]*//g' $file |awk '/Array/,/Enclosure/' |grep "Interface Type:" |cut -d' ' -f3)
				disk_size=$(sed 's/^[ ]*//g' $file |awk '/Array/,/Enclosure/' |grep -Poz "Interface Type:.*\nSize:.*" |grep "Size:" |cut -d' ' -f2)
			fi
		done
		rm -f scality_raidinfo-*
	done
else
	while read line
	do
		[ "$line" == "" ] && continue
		disk_name=$(echo $line | cut -d ' ' -f 1)
		disk_size=$(echo $line | cut -d ' ' -f 2)
		[ ! -f "/sys/class/block/${disk_name}/device/model" ] && continue
		disk_model=$(</sys/class/block/${disk_name}/device/model)
		disk_vendor=$(</sys/class/block/${disk_name}/device/vendor)
	done <<< "$(lsblk --list -n -o name,size)"
fi

echo
echo ---
echo manufacturer: $manufacturer
echo product_name: $product_name
echo os_release: $os_release
echo os_disk: $os_disk
echo os_disk_model: $os_disk_model
echo os_disk_vendor: $os_disk_vendor
