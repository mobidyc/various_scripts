#!/bin/bash

Usage()
{
	cat << EOT
	$0 <y|n>
		y: will activate the auto balance feature
		n: will deactivate the auto balance feature
EOT
	exit ${1:-0}
}

balance_auto=

while [ "$#" -ne "0" ]
do
	case $1
	in
		"y") balance_auto=1 ; shift 1 ;;
		"n") balance_auto=0 ; shift 1 ;;
		"-h"|"--help") Usage ;;
		*) Usage 1 ;;
	esac
done

if [ -z "$balance_auto" ] ; then
	Usage 1
fi

def_file=$(mktemp)
rings=$(ringsh supervisor ringList)
for ring in $rings
do
	ringsh supervisor dsoConfigSet $ring move_auto $balance_auto
	if [ "$balance_auto" = "0" ] ; then
		ringsh supervisor ringTasks $ring |grep -w move |while read -r node task a b c d tid
		do
			ip=$(echo $node |cut -d: -f1)
			port=$(echo $node |cut -d: -f2)
			tid=$(echo $tid |cut -d= -f2)
			echo "supervisor nodeStopTask $ip $port $tid" >> "$def_file"
		done
		ringsh -f "$def_file"
		cp /dev/null "$def_file"
	fi
done

rm "$def_file"

