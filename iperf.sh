#!/bin/bash
cp /dev/null /tmp/iperf.results
IPs=${@}
SSHOPTS="-o StrictHostKeyChecking=no"

for i in $IPs ; do ssh $SSHOPTS $i "iperf -s -D -p 64999" ; done &>/dev/null

for i in $IPs
do
  for j in $IPs
  do
    [ "$i" == "$j" ] && continue
    ssh $SSHOPTS $i "iperf -c $j -P 15 -p 64999" | \
      fgrep '[SUM]' | \
      awk -v src=$i -v dst=$j '{
        print src "->" dst ": " $(NF-1), $NF
      }' | \
      tee -a /tmp/iperf.results
  done
done
for i in $IPs ; do ssh $i "killall iperf" ; done &>/dev/null
