#!/bin/bash
 
cp /dev/null /tmp/iperf.results
IPs=${@}
SSHOPTS="-o StrictHostKeyChecking=no"
 
for i in $IPs ; do ssh $SSHOPTS $i "iperf3 -s -D -p 64999" &>/dev/null ; done
 
for i in $IPs
do
  for j in $IPs
  do
    [ "$i" == "$j" ] && continue
    # echo "iperf test src=$i dst=$j"
    ssh $SSHOPTS $i "iperf3 -c $j -P 15 -p 64999" | \
      fgrep '[SUM]' | \
      egrep 'sender|receiver' | \
      awk '{printf("%s %s %s ", $6, $7, $NF)}END{print ""}' | \
      awk -v src=$i -v dst=$j '{
        if ($3 ~ /sender/) {
          print src "->" dst ": " $1, $2
        } else {
          print src "->" dst ": " $4, $5
        }
      }' | \
      tee -a /tmp/iperf.results
  done
done
for i in $IPs ; do ssh $SSHOPTS $i "killall iperf3" &>/dev/null ; done
